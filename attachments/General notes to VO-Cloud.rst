General notes to VO-Cloud
=========================

Job ve stavu PENDING dostane URL kde najde ZIP se zdrojovyi daty

Při přechodu do RUNNING  stahuje data od launcheru

V params.zip v JSON konfiguraku jsou relativni cesty k datum etc.


Preprocessing
=============

Ocekava strukturu dat stejnou jako spektra ... slozky jsou tridy, obsahuji data v podobe raw FITS souboru.

Vystupem je CSV kde prvni sloupec je ID a posledni sloupec je LABEL (int). -- Toto CSV je vstupem do
vsech klasifikacnich modelu


Poznamky
========

SSA specifikuje ze pro DatLink operace (transformace) je nutne (v pripade FITS souboru) mit data ve 
formatu binarni tabulky.
