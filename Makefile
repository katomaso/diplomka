all:
	rm -f DP_Peterka_Tomas_2015.pdf
	makeglossaries DP_Peterka_Tomas_2015
	latexmk -bibtex -f -pdf -dvi- DP_Peterka_Tomas_2015
#	latexmk -c -pdf DP_Peterka_Tomas_2015

test:
	pdftex DP_Peterka_Tomas_2015.tex
	bibtex DP_Peterka_Tomas_2015
	rm -f DP_Peterka_Tomas_2015.pdf
	pdftex -halt-on-error DP_Peterka_Tomas_2015.tex
	rm -f DP_Peterka_Tomas_2015.pdf
	pdftex -halt-on-error DP_Peterka_Tomas_2015.tex


