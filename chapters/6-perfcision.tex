In this chapter we describe development of two models based on deep neural networks. The models were
build to match the given datasets. The first model is a proof of concept to create a simple
functional neural network and to measure its performance on CPU and GPU. The other model is a
convolutional network which unleash the possibilities of deep networks.

\section{GPU Multi-Layer Perceptron} % (fold)

We tested a simple multi-layer perceptron model on standard astronomical dataset SDSS DR12 and
compared our results with classification performed by the authors of scikit-learn\cite{scikit-learn}.
We used the same download method as described at
astroML\footnote{\url{http://www.astroml.org/user_guide/datasets.html}}. The dataset is designed
for classification of stars and quasars based on multiple features from which were selected
magnitudes in standard SDSS filters. The dataset is kindly offered by Sloan Digital Sky Survey
(SDSS)\footnote{\url{http://www.sdss.org/}}.

\subsection{SDSS Quasar-Star dataset} % (fold)
\label{sub:sdss_quasar_star_dataset}

We followed data preparation described by scikit-learn\cite{scikit-learn}. The dataset comes in two
separate files -- for every category one file. Table \ref{prfacc:table:scikit-dataset} shows brief
statistics of the dataset and another one, which was used for the naive bayes classifier.

\begin{figure}
\centering
\begin{tabular}{l r r r r}
                    & Total    & Stars (S)  & Quasars (Q) & S/Q ratio \\
Training            & 390,040  &   294,814  &     95,226  &    3:1    \\
Testing             &  43,003  &    32,446  &     10,557  &    3:1    \\
                    & & & & \\
Naive Bayes (train) & 500,000  &          - &         -   &   14:1    \\
\end{tabular}
\caption{Brief statistics of the quasar-star datasets.}
\label{prfacc:table:scikit-dataset}
\end{figure}

All classifiers except the Naive Bayes were tested on the same dataset as our model. Naive Bayes,
however, uses data from the same source (SDSS) but of a different size. Its dataset is a mixture of
700,000 objects out of which 500,000 is used for training and the ratio of stars and quasars
approximately 14:1.

Both datasets are tabular values of magnitudes in a few different filters altogether with labels.
Magnitudes are scalar values which denote radiation flux relative to a reference object. The Figure
\ref{impl:fig:sdss_filter} shows a spectrum of an object (in our case Vega) and as a background
there are filters in different wavelengths.

\begin{figure}[htp]
	\centering
	\subfloat[Filters]{%
		\includegraphics[width=0.48\textwidth]{drawings/plot_sdss_filters.png}%
		\label{impl:fig:sdss_filter}}
	\hfill
	\subfloat[Redshift of spectra]{%
		\includegraphics[width=0.48\textwidth]{drawings/plot_sdss_redshift.png}%
		\label{impl:fig:sdss_redshift}}
\end{figure}

The filters, used in SDSS, are ultraviolet(u), green(g), red(r) and infra red(i, z). In order to get
magnitude we need to compute photon flux $I$ first. It is an integral of object spectrum multiplied
by filter's throughput as shown in equation \ref{eq:flux}. Since we compute photon flux for every
filter, we denote it by its filter sign (eg. $I_u$).

\begin{equation}\label{eq:flux}
	I_x = \int_0^\infty f_x(\lambda)S_\nu(\lambda)\frac{d\lambda}{\lambda}
\end{equation}
%
where $x \in {u, g, r, i, z}$ or other set naming filters used for observation, $f_x(\lambda)$ is
the filter value at wavelength $\lambda$ and $S_\nu(\lambda)$ is the photon flux at the wavelength
$\lambda$. Equation \ref{eq:magnitude} shows the final transformation of a flux value into
\emph{magnitude}~$m_1$
%
\begin{equation}\label{eq:magnitude}
	m_1 - m_{ref} = -2.5\log_{10}(\frac{I_x}{I_{ref}})
\end{equation}
%
Magnitude is a historical relict which is still used. It is defined as negative logarithm so it has
actually lower value for brighter object than a reference object. It is sometimes called an apparent
brightness. The astronomers use star Vega as the reference object. Following this point of
reference, the Moon is given a magnitude of -13, while Venus -5 and Sirius -1.5.

Different groups of objects usually have a different \gls{redshift} due to different cosmological
distances. For example galaxies has lower redshift than quasars. A redshift moves the spectral line
on the x-axis. It doesn't introduce any changes in the shape of spectra as shown in Figure
\ref{impl:fig:sdss_redshift}.

We constructed two different feature sets. First dataset consisted from unprocessed magnitudes -
therefore having 5 columns of features. The second dataset was constructed based on domain knowledge
of differences between quasars and stars in their spectra. The features were subtractions of
magnitudes from the subsequent filters $(u-g, g-r, r-i, i-z)$. The difference between following
features identifies the positioning of a spectra according to the filters thus it can guess the
overall \gls{redshift}. This is the best known feature selection with this dataset. We are using the
second dataset most of the time so we can compare with astroML results.

% subsection sdss_quasar_star_dataset (end)

\subsection{Model}
\label{sub:simple_mlp_solver}

We constructed a simple multi-layer perceptron network for comparison with other classifiers which
were used on the same dataset. This implementation also provides a baseline for performance and
accuracy testing for our other models.

The first version has only one layer. We didn't introduce any scaling nonlinearities because the
classes are distinct based on difference between intensities in subsequent filters so any scaling
down would damage the accuracy of the classification.

\begin{figure}[htp]
	\centering
	\subfloat[Simple MLP solver for QS problem]{ %
	\begin{minipage}[c][1\width]{%
	   0.4\textwidth}
		\begin{tabular}{ l r }
		   \multicolumn{2}{c}{\textbf{Solver}}\\
		   iterations     &  10,000           \\
		   learning rate  &     0.3           \\
		   step interval  &     300           \\
		   gamma          &     0.9           \\
		   batch size     &   5,000
		\end{tabular}%
		\end{minipage}}%
	\subfloat[Simple MLP model for QS problem]{%
	\begin{minipage}[c][1\width]{%
	 0.6\textwidth}\label{fig:mlp_arch}
	 \includegraphics[width=0.9\textwidth,clip=true,trim=1.3cm 10cm 5cm 1cm]{drawings/2layers.pdf}
	\end{minipage}}%
\end{figure}

The first fully-connected layer with 10 dimensional output showed as sufficient. Best practices
published by the DAME team \cite{DAME.Brescia:2014} say that number of neurons should descend with
layers and the starting value, which is equal to the number of neurons in the second layer, should
be $2N-1$ where $N$ is number of input neurons so we are very close to the recommended value which
would be in our case 7. Our data are separable by a polynomial of first degree in 3D space.

\subsubsection{Accuracy} % (fold)
\label{ssub:accuracy}

We were building the model incrementally from the input layer. The accuracy was measured after every
step and we ended up with architecture shown in figure \ref{fig:mlp_arch}. With only one hidden
layer and no non-linearities in the network we were able to achieve 96.3\% accuracy with loss=$0.3$.
Additional linear layers did not improve average result. However additional non-linearity was able
to improve the loss down to $0.1$ and accuracy up to 98.59\%. More surprisingly a ReLU unit improved
average result more than a hyperbolic-tangent unit.

\begin{figure}[htp]
	\centering
	\subfloat[2-layer network with total accuracy 98.59\%.]{ %
		\begin{tabular}{ l r r r }
					   &  Star   &   Quasar  &   Accu. \\ \hline
		Star           &  33,766 &      180  &   99.5\%   \\
		Quasar         &     456 &   10,598  &   95.9\%   \\
		\end{tabular}}%
	\hfill
	\subfloat[3-layer network with total accuracy 99.1\%.]{ %
		\begin{tabular}{ l r r r }
					   &  Star   &   Quasar  &   Accu. \\ \hline
		Star           &  33,751 &      195  &   99.4\%   \\
		Quasar         &     215 &   10,839  &   98.1\%   \\
		\end{tabular}}%
	\caption{Confusion matrix of simple QS model. Rows denote true class, columns output of the network.}
\end{figure}

With the addition of another layer we noticed an increase in accuracy up to 99\%. The accuracy was
always around this level and with fine-tuning of the weights and number of outputs we were able to
increase up to 99.1\%. The network with more layers is able to distinguish between more classes
while the simple 2-layer network was more precise with one class. Any other additional layer
worsened the result. There are two possible explanations. The first is that the gradient dissolves
in too many layers. The second is that more layers bring higher dimensionality in which the data
might not be that easily separable.

With the introduction of another layer we switched to the first dataset because the second dataset
is produced as a linear combination of the first one. But we never reached the precision of the
second dataset. Even with a \texttt{dropout} layer and uniform weights initialization in a wide
interval such as $(-3, +3)$ the net wasn't able to produce the correct linear combination.

We compared our multi-layer perceptron classifier with \emph{Gaussian Naive Bayes} whose results are
available online\footnote{\url{http://www.astroml.org/sklearn_tutorial/classification.html}} and
with classifiers originally used by \texttt{scikit-learn} on this dataset. The values in the table
\ref{fig:astroml_vs_us} are the best obtained from a few iterations.

\begin{figure}[htp]
\centering
\begin{tabular}{ l r r r }
          Classifier &  Stars  &   QSOs    & Total \\ \hline
       Decision tree &   99\%  &   100\%   &  99\% \\
                 kNN &   98\%  &   100\%   &  99\% \\
\textbf{our GPU MLP} &   \textbf{99\%}  &   \textbf{98\%}   &  \textbf{99\%} \\
           GMM Bayes &   75\%  &   100\%   &  75\% \\
         Naive Bayes &   99\%  &    14\%   &  94\% \\
 Logistic regression &   75\%  &     0\%   &  75\% \\
\end{tabular}
\caption{Comparison of accuracy of different classifiers}
\label{fig:astroml_vs_us}
\end{figure}

The skew in Naive Bayes' dataset explains relatively bad metrics of the classifier. The skew in
training data is forcing the model to put more stress on one class. There are ongoing debates if
training samples should have homogeneous distribution of classes with sacrifice of training data.
The results show that even bigger amount of data did not save the model from biasing. Our and some
other astromML's models handle smaller skew in the data gracefully. The results show that two
bottom-line models could not handle the skew well.

% subsubsection accuracy (end)


\subsubsection{Performance} % (fold)
\label{prfcy:ssub:mlp:performance}

First we investigated how the performance changes with respect to amount of data in one batch. The
results show that both CPU and GPU implementations scale linearly with different coefficients. We
computed those coefficients and put them into an equation which shows time demand based on batch
size $S_B$
\begin{wrapfigure}{r}{0.5\textwidth}
\begin{center}
	\includegraphics[width=0.5\textwidth,clip=true,trim=1.5cm 1.5cm 1.5cm 19cm]{charts/chunksize-time.pdf}
	\caption{Performance based on chunk size.}
	\label{fig:chunksize_time}
\end{center}
\end{wrapfigure}
%
\begin{align*}
	T_{CPU} &= 55 * S_B \\
	T_{GPU} &= 6.2 * S_B
\end{align*}
Since our network accommodates most of the common layers (fully-connected, ReLU, tanh) we can say
that those coefficients approximate GPU speed-up over CPU. Thus we can state that GPU implementation
of a simple MLP network is about 9 times more efficient than CPU implementation.

Linear scale with respect to block size shows that Caffe does not use \texttt{CUDA streams}. CUDA
streams are way of asynchronous data transfer and computations. If CUDA streams were involved then
we would get sub-linear time with special case where the time of data transfer is equals to
computations and in this case the time should stay constant for small range of chunk size.

In order to further investigate performance differences between CPU and GPU implementations we
present a breakdown by layers, implementations and phases in table \ref{table:layer_breakdown}.

\begin{figure}[htp]
	\centering
	\begin{tabular}{l c r r}
		Layer                   & phase & CPU[ms]  & GPU[ms] \\ \hline
		\multirow{2}{*}{train}	& F     & 3.10     & 2.65 \\
								& B     & 0.00     & 0.00 \\ \hline
		\multirow{2}{*}{  ip1}	& F     & 13.70    & 1.37 \\
								& B     & 20.88    & 3.03 \\ \hline
		\multirow{2}{*}{relu1}	& F     & 5.02     & 1.28 \\
								& B     & 10.26    & 1.12 \\ \hline
		\multirow{2}{*}{  ip2}	& F     & 10.70    & 1.32 \\
								& B     & 19.26    & 3.22 \\ \hline
		\multirow{2}{*}{tanh1}	& F     & 17.40    & 1.36 \\
								& B     & 1.51     & 1.25 \\ \hline
		\multirow{2}{*}{  ip4}	& F     & 7.43     & 1.27 \\
								& B     & 14.27    & 3.13 \\ \hline
		\multirow{2}{*}{ loss}	& F     & 197.73   & 12.30 \\
								& B     & 3.81     & 0.17 \\
	\end{tabular}
	\caption{Time breakdown by layers and implementation. Phase specifies further the run, if it was F resp. B forward resp. backward run.}
	\label{table:layer_breakdown}
\end{figure}

In terms of classification of unknown object we have measured average throughput 11,400 objects per
millisecond which is equal to dataflow of 760 MB/s. This value is for forward pass exclusively and
therefore it states only the classification throughput, not the performance while training.

We were about to compare performance of our \acrshort{MLP} model with DAME. Despite immense help by
Dr. Massimo Brescia we were unable to load our data into DAMEWARE and perform any benchmarks. DAME
offers massive parallel version of \acrshort{MLP} model with optional quasi newton algorithm (QNA)
which would be great for comparison. We did try again for our next convolutional model with slightly
better result.

% subsubsection performance (end)

% section quasar_star_dataset (end)



\section{GPU Convolutional Network} % (fold)
\label{impl:sec:gpu_convolutional_network}

We introduce a new technique for classifying astronomical spectra. The idea comes from image
classification where the convolutional networks are heavily used. Convolutional networks were
already used for classification of spectra \cite{hala-konvoluce} with accuracy over 90\% but with a
very expensive preprocessing phase. The preprocessing they used was to convert spectra into 60x60
pixel images. Those images were fed into standard \texttt{LeNet-5} and the network achieved accuracy
96.5\%. However, we cannot compare our accuracies because they used a different dataset. In order to
put this classification method to a practical use we have built a model which can deal with spectra
in the form as they are available through \gls{IVOA} tools. We imagine the spectra as 1D images
where we already have brightness of pixels. The brightness, which is represented by flux, needs to
be normalized to continuum but that is usually done in spectra processing pipeline. Therefore our
model is able to classify massive amounts of continuum normalized spectra obtained from standard
sources without any further preprocessing.

\subsection{Ondřejov's Be-stars dataset} % (fold)
\label{impl:sec:ondrejov_spectral_dataset}

The classical Be stars are non-supergiant B type stars whose spectra have or have had at some time,
one or more emission lines in the Balmer series \cite{be-stars}. In particular the $H_\alpha$
emission is the dominant feature in spectra of these objects. Characteristic for Be stars are the
single or double-peak profiles and sometimes so called shell lines deep absorptions in centre of
the emission. Figure \ref{fig:spectrum_classes} show spectral shapes of two example objects for each
category. The upper half of every image is a complete spectra ranging from $6200$ to $6800$
Ångström. The bottom half shows the interesting part in greater detail. The y-axis is normalized
flux.

\begin{figure}[htp]
	\centering
	\subfloat[Be star with emission, class \#0]{
		\includegraphics[width=0.3\textwidth,clip=true,trim=2cm 1cm 2cm 1cm]{charts/spectra/1-hd50820_preview.png}}
	\hfill
	\subfloat[Be star, double peak emission, class \#1]{%
		\includegraphics[width=0.3\textwidth,clip=true,trim=2cm 1cm 2cm 1cm]{charts/spectra/2-v2162cyg_preview.png}}
	\hfill
	\subfloat[Be star, absorption inside emission, class \#3]{%
		\includegraphics[width=0.3\textwidth,clip=true,trim=2cm 1cm 2cm 1cm]{charts/spectra/4-4aql_preview.png}}

	\subfloat[Be star, chaotic emission, class \#4]{%
		\includegraphics[width=0.3\textwidth,clip=true,trim=2cm 1cm 2cm 1cm]{charts/spectra/5-v341sge_preview.png}}
	\hfill
	\subfloat[Ordinary absorbing stars, class \#2]{%
		\includegraphics[width=0.3\textwidth,clip=true,trim=2cm 1cm 2cm 1cm]{charts/spectra/3-hd38091_preview.png}
		\hspace{0.33cm}
		\includegraphics[width=0.3\textwidth,clip=true,trim=2cm 1cm 2cm 1cm]{charts/spectra/3-hd18131_preview.png}}
	\caption{Example spectra lines}
	\label{fig:spectrum_classes}%
\end{figure}

Our dataset contains 1696 spectra samples of stars divided into 5 classes. The distribution of
classes is shown in table \ref{tbl:ondrejov_dataset_classes_dist} in row ``Total''. All classes
except \#2 are Be stars. Each spectrum composes from approximately 2000 flux values around
$H_{\alpha}$ line $(656.28$ nm $= 6562.8$ Å $)$. The spectra in our dataset were binned into
$0.15$ Å wide bins which unifies all spectra with respect to x-axis. There is no need for
wavelengths to be stored in the data so every record is only 1 dimensional array of flux values.

\begin{figure}[htp]
\centering
\begin{tabular}{ l r r r r r r }
               & records &   \#0   &   \#1   &   \#2   &   \#3   &   \#4 \\ \hline
Total          &  1,696  &   178   &   172   &  1,159  &    56   &   131 \\
Training       &    663  &   163   &   152   &    180  &    52   &   116 \\
Testing        &     94  &    15   &    20   &     40  &     4   &    15 \\
\end{tabular}
\caption{Ondřejov's datasets class distributions}
\label{tbl:ondrejov_dataset_classes_dist}
\end{figure}

There is clear dominance of class \#2 in the raw dataset because this class is assigned to generic
absorption stars which are not Be stars. The model tends to put more weight to the class thus skew
the results of classification. It is always better to sacrifice some data in favour of homogeneous
distribution of training samples. We redistributed the classes for training and testing samples as
shown in table \ref{tbl:ondrejov_dataset_classes_dist}.

% subsection ondrejovs_bestars_dataset (end)

\subsection{Model} % (fold)
\label{sub:model}

The base for this model was taken from the previous multi-layer perceptron. We have constructed two
cNN models. First model has one wide convolutional layer and the second has two narrow
convolutional layers. In both cases it was necessary to put a hyperbolic tangent layer right in
front of the output layer otherwise the output grew too high and made its loss function to diverge.
Hyperbolic tangent scales any number between $(-1, 1)$ so it is ideal for scaling data before
computing a loss.

Convolutional layer is usually followed by a pooling layer to sharpen feature map by removing
partial fit of features. We have chosen max-pooling layer which works similarly to convolution but
instead of a convolution matrix it uses \texttt{max} function over a region of neurons.

As in the previous MLP model, it was necessary to introduce some non-linearity. We have used ReLU
unit which is preferred over other non-linear units for its speed. A non-linear unit is usually
placed between last pooling layer and before first fully-connected layer. We followed this practice.
The resulting network is depicted in figure \ref{fig:cnn-diag}.

\begin{figure}[htp]
	\centering
	\subfloat[Solver used for convolutional network]{ %
	\begin{minipage}[c][1\width]{0.4\textwidth}
		\begin{tabular}{ l r }
		   \multicolumn{2}{c}{\textbf{Solver}}\\
		   iterations     &  10,000           \\
		   learning rate  &     0.3           \\
		   step interval  &     300           \\
		   gamma          &     0.9           \\
		   batch size     &   5,000           \\
						  &                   \\
		\end{tabular}%
	\end{minipage}}%
	\subfloat[Architecture of convolutional network for classifying spectra.]{%
	\begin{minipage}[c][1\width]{0.6\textwidth}
	\includegraphics[width=0.75\textwidth,clip=true,trim=1.5cm 6cm 5cm 1cm]{drawings/cNN-diagram.pdf}%
	\label{fig:cnn-diag}
	\end{minipage}}
\end{figure}


\subsubsection{Accuracy} % (fold)
\label{subs:accuracy}

During training, we experienced that convolutional networks are very sensitive to initial learning
rate. Convolutional networks require smaller initial learning rate and slower decay. The
experiments show that $0.1$ is optimal initial value

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth,clip=true,trim=1.5cm 1.5cm 1.5cm 19cm]{charts/lr-loss.pdf}
	\caption{Progress of loss function based on learning rate.}
	\label{fig:lr-loss}
\end{figure}

The kernel size is very important factor in optimization of a convolutional layer. The most
complicated spectral profile of any Be-star is double peak. If a convolutional network should match
the important features it needs to overcome the small disturbing waves which are presented, for
example, in spectra of class \#2. The ideal width of kernel is wide enough to smooth turbulent waves
but narrow enough to capture the top ``saddle'' point between two peaks.
Convolutional layer should match any possible shape so one of those shape can be the saddle. The
figure \ref{fig:kernel-loss} shows the dependency between kernel width and a loss function. It
confirms for one layered convolutional networks that kernel has to be rather bigger than smaller in
order to overcome jitter.
%
\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth,clip=true,trim=1.5cm 1.5cm 1.5cm 18cm]{charts/kernel-loss.pdf}
	\caption{Progress of loss function based on kernel size}
	\label{fig:kernel-loss}
\end{figure}

The reason for having wider kernels in one layered \acrshort{cNN} is that we can not say in advance
how wide the features will be. It is better to let the convolutional layer to extract the important
knowledge by itself. Figure \ref{fig:conv-spectra-features} shows one of the feature maps which was
yielded by our convolutional network. This feature matches negative slope in spectral shape.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.40\textwidth]{charts/conv-feature-descending.pdf}
	\caption{A smaller feature from two-layer convolutional network}
	\label{fig:conv-spectra-features}
\end{figure}

The most successful kernel of width 100 had accuracy 94.7\% with confusion matrix presented in table
\ref{table:conv1-conf}. This value seems to be the limit for one layer convolutional network on our
data. If we train the network further it starts to overfit.
\begin{figure}[htp]
\centering
\begin{tabular}{ r | r r r r r }
 guess:   & \#0  &  \#1  &  \#2  & \#3  &  \#4 \\ \hline
 true \#0 &  95  &    0  &    0  &   0  &   13 \\
 true \#1 &  0   &  128  &    0  &   0  &    7 \\
 true \#2 &  0   &    0  &  272  &   0  &    0 \\
 true \#3 &  0   &    0  &    7  &  21  &    0 \\
 true \#4 &  0   &    7  &    0  &   0  &   94
\end{tabular}
\caption{Confusion matrix for one-layered cNN after 1500 iterations. Accuracy 91.32\%}
\label{table:conv1-conf}
\end{figure}

The patterns in spectra are simple -- a classifier should learn few shapes like slops up and down,
flats and saddle points. Therefore it is crucial not to make the search space too complex. During
training, we observed that ideal number of convolutional features is between 10 and 20.

As we said earlier, it is vital to let the convolutional network converge slowly. During our experiments it took more than 1500 iterations for two-layers cNN to find the right features. The simpler one-layered cNN converged before 1500 iterations where it achieved its maximal accuracy and since then it was slowly overfitting.

After 2000 iterations convergence was almost final and we obtained 99.07\% accuracy with only one
misclassified spectrum as shown in confusion table \ref{table:conv2-conf}.

\begin{figure}[htp]
\centering
\begin{tabular}{ r | r r r r r }
 guess:   & \#0  & \#1  & \#2  & \#3 & \#4 \\ \hline
 true \#0 &  16  &   0  &   0  &  0  &   0 \\
 true \#1 &   0  &  25  &   0  &  0  &   0 \\
 true \#2 &   0  &   0  &  42  &  0  &   1 \\
 true \#3 &   0  &   0  &   0  &  6  &   0 \\
 true \#4 &   0  &   0  &   0  &  0  &  18
\end{tabular}
\caption{Confusion matrix for two-layered cNN after 2000 iterations. Accuracy 99.07\%}
\label{table:conv2-conf}
\end{figure}

% subsubsection accuracy (end)

\subsubsection{Performance} % (fold)
\label{prfcy:ssub:cnn:performance}

We are interested in how convolutional layer performs under bigger data sizes and with different
parameters. As we did in the previous section, we measured performance based on chunk size. It turned
out to be linear again but with different coefficients.
\begin{align*}
	T_{CPU} &= 61.0 * B_S \\
	T_{GPU} &= 0.95 * B_S
\end{align*}

In order to see why the coefficients changed we measured performance of every layer separately. The
results are shown in table \ref{table:conv-time-breakdown}. The table confirms that convolutional
layer is the heaviest for computation. The computed coefficients are valid only for this network
architecture with one convolutional layer. Having more convolutional layers than the fully-connected
ones will bring less synchronization points thus the theoretical speedup can reach up to 110 times.
The overall speed-up by using GPU for the particular network shown in table \ref{table:conv-time-
breakdown} is around 55 times.

\begin{figure}[htp]
	\centering
	\begin{tabular}{l c r r}
	          Layer         & phase &   CPU[ms] & GPU[ms] \\ \hline
	\multirow{2}{*}{train}	&   F   &   2.39    &  2.29  \\
			 				&   B   &   0.00    &  0.00  \\ \hline
	\multirow{2}{*}{conv1}	&   F   &   186.47  &  0.61  \\
			 				&   B   &   138.71  &  1.24  \\ \hline
	\multirow{2}{*}{pool1}	&   F   &   32.45   &  0.83  \\
			 				&   B   &   24.97   &  0.76  \\ \hline
	\multirow{2}{*}{relu1}	&   F   &   0.02    &  0.02  \\
			 				&   B   &   0.05    &  0.02  \\ \hline
	\multirow{2}{*}{  ip1}	&   F   &   0.45    &  0.04  \\
			 				&   B   &   0.84    &  0.07  \\ \hline
	\multirow{2}{*}{ hyp1}	&   F   &   1.02    &  0.02  \\
			 				&   B   &   0.05    &  0.02  \\ \hline
	\multirow{2}{*}{  ip2}	&   F   &   0.40    &  0.04  \\
			 				&   B   &   0.59    &  0.08  \\ \hline
	\multirow{2}{*}{ loss}	&   F   &   0.79    &  0.20  \\
			 				&   B   &   0.01    &  0.06  \\ \hline
	\end{tabular}
	\caption{Time breakdown of convolutional network.}
	\label{table:conv-time-breakdown}
\end{figure}
%
The average throughput of the two-layered cNN is 192 spectra per millisecond which is equal to 1440
MB/s.

We tried to benchmark performance with DAME again, using \acrshort{MLP} model with QNA optimization.
After few modifications to DAME, performed by Dr. Massimo Brescia, we were able to load our data to
the application. Unfortunately any \acrshort{MLP} used model gave us estimate of 3 months to finish.
DAME implementation is not suitable for such a multicolumn data.

% subsubsection performance (end)

% subsection model (end)

% section gpu_convolutional_network (end)



% \section{SDSS Quasar,Star,Galaxy dataset} % (fold)
% \label{impl:sec:quasar_star_galaxy_dataset}

% We followed closely the process of obtaining data as described in \cite{}. We had to query SDSS DR10
% catalog\footnote{\url{http://skyserver.sdss.org/dr10/en/tools/search/sql.aspx}} to obtain similar
% data as written in the article. We performed two queries to obtain 400,000 galaxies and then 400,000
% different objects.

% \begin{figure}[htp]
% 	\begin{verbatim}
% SELECT TOP 400000
%    p.objid,p.ra,p.dec,p.u,p.g,p.r,p.i,p.z,
%    p.psfMag_u,p.psfMag_g,p.psfMag_r,p.psfMag_i,p.psfMag_z,
%    p.modelMag_u,p.modelMag_g,p.modelMag_r,p.modelMag_i,p.modelMag_z,
%    s.specobjid, s.class, s.subclass, s.z as redshift
% FROM PhotoObj AS p
%    JOIN SpecObj AS s ON s.bestobjid = p.objid
% WHERE
%    s.class!="GALAXY" AND s.class!="" AND
%    p.calibStatus_u=1 AND p.calibStatus_g=1 AND
%    p.calibStatus_r=1 AND p.calibStatus_i=1 AND
%    p.calibStatus_z=1 AND
%    p.clean=1
% 	\end{verbatim}
% \end{figure}
% %
% The other query to obtain 400,000 galaxies differs in the first line of \texttt{WHERE} clause
% %
% \begin{verbatim}
% WHERE
%     s.class="GALAXY" AND
% \end{verbatim}

% Dataset contains 3602210 rows. There are 3,409,356 (94.6\%) unknown objects in the dataset which are
% denoted by value $-2^{63}$ in \texttt{specObjID} column.

% \begin{figure}[hp]
% 	\centering
% 	\subfloat[Brief statistics of the raw QSO datatset]{ %
% 		\begin{tabular}{l r r}
% 			\textbf{Column}	&	\textbf{mean}		&	\textbf{STD} \\ \hline
% 			objid			&	\multicolumn{2}{l}{ID} \\
% 			ra				&	188.73	&	97.11 \\
% 			dec				&	21.14	&	22.18 \\
% 			psfMag\_u		&	21.98	&	1.11 \\
% 			psfMag\_g		&	21.57	&	0.95 \\
% 			psfMag\_r		&	21.12	&	0.87 \\
% 			psfMag\_i		&	20.85	&	0.83 \\
% 			psfMag\_z		&	20.63	&	0.85 \\
% 			modelMag\_u		&	21.97	&	1.15 \\
% 			modelMag\_g		&	21.51	&	0.95 \\
% 			modelMag\_r		&	21.03	&	0.86 \\
% 			modelMag\_i		&	20.76	&	0.82 \\
% 			modelMag\_z		&	20.55	&	0.86 \\
% 			specObjID		&	\multicolumn{2}{l}{TYPE\_ID} \\
% 			subclass		&	\multicolumn{2}{l}{STRING} \\
% 			z				&	1.68	&	0.86 \\
% 			qualityFlag		&	0.14	&	0.35 \\
% 		\end{tabular}} %
% 	\hfill
% 	\subfloat[Some columns in more detail]{ %
% 		\begin{tabular}{r r}
% 		\multicolumn{2}{c}{\textbf{objid (column 1)}} \\
% 		\multicolumn{2}{r}{3,602,210 distinct values} \\
% 		& \\
% 		\multicolumn{2}{c}{\textbf{specObjID (column 13)}} \\
% 		\multicolumn{2}{r}{192,855 distinct values} \\
% 		& \\
% 		\multicolumn{2}{c}{\textbf{subclass (column 14)}} \\
% 				value      &  count \\ \hline
% 				broadline  & 157,336 \\
% 	  starburst broadline  &   5,512 \\
% 			agn broadline  &     312 \\
% 					  agn  &     294 \\
% 	starforming broadline  &     198 \\
% 				starburst  &     130 \\
% 			  starforming  &      38 \\
% 		\end{tabular}} %
% \end{figure}
% section quasar_star_galaxy_dataset (end)


% \paragraph{Random Forest} % (fold)
% \label{par:random_forest}

% was first introduced by \cite{Breinman:2001} and since then it became a buzz-word in todays science.
% The method builds trees using bagging principle with usage of (either decision or regression) trees.
% There is a difference in selection of training data (compared to building a single tree) where each
% tree is computed (using Gini index) by randomly selected dimensions of the data.

% paragraph random_forest (end)
% section ensemble_methods (end)
