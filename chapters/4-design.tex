We have designed a general purpose classifier using CUDA technology. First, we planned to implement
the classifier from scratch because there were no publicly available implementations of parallel
deep neural networks. Therefore sections \ref{design:sec:cuda} and \ref{design:sec:neurons} contain
brief description of CUDA and thorough description of many types of neurons and layers. Due to
current fast progress in the field of deep learning a new thesis emerged and brought an efficient
implementation of cNN on GPUs called Caffe library, developed by Computer Vision Group, Berkeley
University, California\cite{jia2014caffe}. Their design is very elegant and we will describe it the
following sections. We decided to build our classifier on top of Caffe as a mature technology,
recently adapted by NVIDIA, with bright future.

Our classifier is required to read various file formats and support many types of input data. It
ought to be easily deployable in cloud and scheduled with \texttt{VO-cloud} (section
\ref{design:ssub:vo_cloud}) job scheduler. Another important feature is a flexible definition of
neural networks. Every model has to be adjusted to fit its input data so the classifier is also
required to have either fully automatic adoption to inputting data or really simple manual
configuration.


\section{Used technologies} % (fold)
\label{design:sec:used_technologies}

One requirement is implementation using NVIDIA CUDA toolkit which follows standard C99.
\texttt{C++11} was chosen as the application language. It offers modern features, speed and direct
communication with CUDA routines so it is possible to fine-tune the resulting classifier.

The solution should be platform independent. Since our technological demands are very humble, we can
get by with boost and standard libraries, which will be merged together in \texttt{C++14}
specification.

The build system should be platform agnostic too. We chosen CMake partially because it is used by
Caffe itself. We even introduced the same build flags as Caffe has, so the build configuration of
both products is the same.

% section used_technologies (end)


\section{Application architecture} % (fold)
\label{design:sec:architecture}

We need to be able to seamlessly access many input formats such as CSV, VO-tables and FITs files.
Since we are using parts of Caffe library we are designing a thin wrapper which will make
deployment in cloud possible. In order to follow ``loose coupling and high cohesion'' we split
application into three modules where every module exports a factory function.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.9\textwidth]{drawings/class-diagram-vodigger.pdf}
	\caption{Class diagram of future application}
	\label{design:fig:desing}
\end{figure}

This architecture gives us the opportunity to easily extend input routines by adding for example
new source for HDFS if the application will be successful.

The class \texttt{Source} does not need to be optimized because it is purely an utility class to
unify all possible sources. The high demands will be put on Feeder and Solver. Feeder has to be able
to handle huge input files (possibly terabytes). Solver has to be able to efficiently update model's
parameters and therefore implemented at least partially in CUDA.

% section architecture (end)

\section{VO-Cloud} % (fold)
\label{design:ssub:vo_cloud}

The final application is expected to work inside VO-Cloud. That dictates usage of JSON configuration
files and eventually implementing a deployment script which will register our service into a master
server.

VO-Cloud is a scheduling server with web interface currently being developed by a small team of
students of informatics lead by Dr. Petr Škoda at Ondřejov observatory\cite{koza-2015}. It aims to
become a whole processing pipeline for astronomical spectra obtained from external archives using
protocols specified by \gls{IVOA} standards. At the end of the transformation pipeline, there will be
a choice to select a machine learning algorithm for building a prediction model.

% section vo_cloud (end)


\section{Neural Network Architecture}

\paragraph{Learning rate decay} is a useful feature against overfitting of a network. It lowers the
learning rate with time. There are many ways how to implement it but the simplest, which we are
using, is called \texttt{step LR decay} meaning that in every $N$ steps the learning rate is
multiplied by a factor $\gamma < 1 $.

\paragraph{Regularization (a.k.a. weight decay $\xi$ )} is another technique which prevents model
from overfitting by lowering complexity of layer's weights. The main idea is similar to Occam's
razor in adding a constraint in order to cut out unwanted complexity. The regularization is
performed by adding transformed weights to the error function as shown in equation
\ref{eq:regularizaion}. Thereafter the model tends to keep its weights low because they are
increasing the error function.

\begin{equation} \label{eq:regularizaion}
	\vec{E_i} = \vec{E_i} + \xi*||\vec{w}||
\end{equation}
\begin{equation}
	\text{where}  ||\vec{w}|| = \begin{cases} \nonumber
								L1  & ||\vec{w}|| = |w_{i}| \\
								L2  & ||\vec{w}|| = w_{i}^2 \\
								\end{cases}
\end{equation}
where $i$ denotes $i$-th layer, $\vec{E_i}$ vector of partial errors.

\paragraph{Learning momentum} helps to overcome local minima in loss function. The network needs to
keep track of historical changes to weights and take those into account with strength $s \in (0,
1)$.

\paragraph{Batch processing} is a technique for speeding up and possibly parallelizing the whole
network. When a batch of data is sent to the network, the parameters update happens only when the
batch is finished propagating. The gradient is computed for every datum from the batch but the
application of the gradient happens only once. That allows the gradients to be computed
asynchronously. More importantly, some layers can operate on one datum identically as on a batch of
data because there are no dependencies between a value and the neighbouring values. For example
sigmoid, hyperbolic or ReLU transformation are simply applying their own function on a value no
matter what the value means. It can even be implemented as a in-place transformation.

\paragraph{Weights initialization} is commonly known to not be very important. The default form of
weights initialization is by using a random numbers generator. If we imagine weights as a matrix
then its eigenvalues has to be close to 1. If the eigenvalues differs greatly from 1 then the
information from input will either disappear or exponentially increase leaving nothing but noise
\cite{thetalkingmachines2}.

\paragraph{Error contribution} is a solution to the biggest problem of deep neural networks --
gradient decay. Gradient decay describes the fact that error, which is back-propagated through
layers, slowly disappears. It is caused by dividing the error between many neurons in every layer.
Caffe indeed has implemented a solution to this problem that any layer can act as a \emph{loss
contributor} with a certain ratio. Hence it is possible to train even very deep networks and ensure
that the gradient will not decay in the first few bottom layers. In the configuration file, it is
controlled by the parameter \emph{loss\_weight}$ \in [0,1]$.


\section{Neurons} \label{design:sec:neurons}

Neuron is an object holding \emph{activation function} and input and output value. The types of
neurons do not distinguish just by their activation function but, as a consequence, by the method of
learning and therefore capability of classifying.

\subsubsection{Terms} \label{design:ssub:terms}

Indices $i$, $j$ refers to two subsequent neurons where $j$ is the later one. Index $j$ can be
viewed as an output neuron and $i$ as a neuron in the hidden layer.\\
$t$ is the expected output of a neuron.\\
$y$ is an output of a neuron (result of it's activation function).\\
$x$ is an input of a neuron from \emph{one} other neuron therefore $x_j$ is equal to $y_i$.\\
$w_{ik,jl}$ is a weight of a connection which transports value $y_i$ from the $k$-th neuron to the
$l$-th neuron in the higher level\\
$z_{jl}$ is a total input to the $l$-th neuron given by
\begin{align}
	z_{jl} &= \sum_{n \in N}{w_{in,jl} y_{in}} \nonumber \\
	       &= \sum_{n \in N}{w_{in,jl} x_{jl}} \nonumber
\end{align}

% subsubsection terms (end)

The first kind of artificial neuron is known as McCulloch-Pitts neuron developed around 1943. It takes into account bias $b$ when producing its output $z$ as
$$ z = w_{0n}b + \sum_{n \in N}{w_{in,jl} x_{jn}} $$
and has a non-differentiable binary activation function
\begin{equation*}
    y = \begin{cases}
               0      & if z < 0\\
               1      & \text{otherwise} \\
        \end{cases}
\end{equation*}
Learning method for this type of neuron is updating weights by input values.
$$ \Delta \vec{w} = \sgn(t-y) * \vec{x} $$
The learning method guarantees that the weights are getting always closer to the desired weights.
The McCulloch-Pitts neurons are still in use because of how fast they can learn even with huge amount of inputs.
Therefore companies like Google are still using them.


\paragraph{Perceptron}
It is a neuron with linear, differentiable activation function which is simple weighted sum
of it's inputs
\begin{equation}
	y = \sum_{n \in N}{\vec{w}^T \vec{x}}
\end{equation}
with lower bound $ \if y < 0 \then 0 \fi $.

The learning method is based on minimizing \emph{residual error} $ E = t - y $ which uses input
value to update weights
\begin{equation} \label{eq:perceptron-learning}
\Delta w_i = \epsilon\,x_i (t-y)
\end{equation}
where $\epsilon << 1$ is a learning rate.

The formula \ref{eq:perceptron-learning} is a specific case of the generic backpropagation formula.
The learning method guarantees that the output is getting always closer to the desired output. The
expressibility of a network compounded by linear neurons is unfortunately very limited. It can't do
more than a linear regression model since the whole network can be described as a linear combination
of inputs.

\paragraph{Non-linear neurons}
They use weighted sum as input to their activation function but the result is everything but linear.
The most common functions are
\begin{itemize}
	\item \textbf{Logistic (a.k.a. Sigmoid)} $ o(x) = (1-e^{-x})^{-1} $ \\
	is the most common neuron in all networks. There are two disadvantages compared to the others.
	First is that it is computationally demanding (learning and evaluating) and the second is that
	it might become easily saturated.
	\item \textbf{Hyperbolic} $ o(x) = \tanh(x) $ \\
	was introduced because it learns faster in certain cases when the data are really distinct (for
	example contrastive images).
	\item \textbf{Rectified Linear Unit (ReLU)} $ o(x) = max(0, x) $ \\
	were introduced because of their performance. They are several times faster than ordinary
	logistics neuron and yet bring similar nonlinearity into the system. Those units were introduced
	by Nair and Hinton \cite{relu}
	\item \textbf{Softmax} $ o(x)_j = e^{x_j} * (\sum_i{e^{x_i}})^{-1} $ \\
	is mostly used as the last layer for the final output. The main reason for that is that since it
	is defined as a sum the derivative of this expression is complicated
	\item \textbf{Dropout} blocks some values from random neurons so they won't contribute to the
	final result. It is used to mitigate over-fitting and it is getting on popularity.
\end{itemize}
%
For better reasoning about the nonlinear units we add a plot of two the most common ones.
\begin{figure}[htp]
	\centering
	\includegraphics[scale=0.5]{charts/nonlin-activations.pdf}
	\caption{Plot of sigmoid and tanh activations}
	\label{design:fig:nonlin-activations}
\end{figure}


\section{CUDA} \label{design:sec:cuda}

\paragraph{Architectures} of CUDA enabled cards changes with time. NVIDIA introduced 4 architectures
so far: Tesla (2008), Fermi (2010), Kepler (2012), Maxwell (2014). Every architecture brought a new
technology; Tesla -- atomic operations, Fermi -- synchronization routines and 3D grid of thread
blocks, Kepler -- unified memory programming (pinned memory with automatic management from the side
of GPU which, if necessary, transfers the memory back and forth), Maxwell -- Dynamic Parallelism
(nested threads, when a device thread can launch new threads so the thread grid can become
heterogeneous).

\paragraph{Threads, Warps and Blocks} are computational units which are controlled by the
programmer. The biggest and physical computational unit of device is the \emph{streaming
multiprocessor} (SM) which accommodates usually few hundreds of cores and possesses small memory
called \emph{shared memory}. The next computational unit in size is a block, which is a virtual
unit. There are up to 16 blocks per SM. A programmer can demand his code to run on up to $2^{16} -
1$ blocks. The smallest units are threads. There is maximum of 1024 threads per block. Threads are
scheduled in warps -- 32 threads together by a warp-scheduler. Warp share instruction counter so if
one thread takes different execution path then it has to NOP through instructions when it does not do
anything. Until all 32 threads has the same execution path, the performance is optimal. Since Fermi
architecture, there are 2 (and further architectures have more) warp schedulers without any
dependency checking between two running warps.

\paragraph{Global memory} is a built-in RAM in the graphic card. Fermi architecture offers up
to 6 GB of RAM with 6 lines (64b each) for reading. It is the main and largest memory on the device.
The trade-off is the speed of the memory because it is the slowest memory on the device.

\paragraph{Shared memory} is the own memory of each core which is used to make intermediate
load/store operations faster. Since the third generation of NVIDIA devices (Fermi architecture), the shared memory size is 64KB per SM. This amount of memory is divided between running blocks so the
actual available size can be 4 -- 32 times smaller. The 64KB can be split in ration 48-16 between
actual shared memory and L1 cache.

\paragraph{Constant memory} read only memory of size 64KB. Read operation from this memory can be
broadcast to group of 16 threads. We should try to serialize any classification model into this
memory. Also since it is constant memory it is heavily cached.

\paragraph{Local memory} is on per-thread level. The size ranges up to 512kB.

\paragraph{Streaming} Two independent queues that stack memory and computational operations. We
will heavily use this technology because the copy and compute operations might take about the same
time. In order to be able to use this feature of graphical cards we need to have access to pinned
memory so we can use \gls{DMA}.

\section{Google Protocol Buffer} % (fold)
\label{design:ssub:google_protocol_buffer}

Google's protocol buffers are serialization format with bindings to many programming languages. The
serialized data can be stored as textual or binary files known as \texttt{protobuf} files. Textual
representation of the data is very similar to JSON except it is statically typed and all relations,
attributes and compound types have to be declared in a definition file. The main advantage is that
parsing files according to a precompiled definition is simple thus fast. The precompiled definition
is what made efficient binary format possible. As the name of the library suggests, it was primarily
designed for protocol definitions and therefore the basic class is called \texttt{Message}. The
whole documentation is available online\footnote{\url{https://developers.google.com /protocol-
buffers/}}. Currently supported languages are C++, Go and Python.

Caffe takes advantage of protobuf's textual and binary formats. The textual files are used as
configuration files. We follow this concept and therefore our classifier requires every model and
solver to be defined in this standard format. We keep those configurations separate from our
application's configuration so it stays framework agnostic. In order to be fully framework agnostic
we have moved some options (such as running mode and numbers of iterations) from Caffe solver
definition to out application configuration. The binary files are used for serialization of models
and solvers from which we use only the model serialization.

% section google_protocol_buffer (end)


\section{Caffe} % (fold)
\label{design:ssub:caffe_neural_layer}

Caffe is primarily a library which provides implementation of many different layers of neural
network in two codes -- CPU and GPU. The biggest advantage is that switching between those two
implementations is done in runtime. No recompilation is needed. Considering that this is such a huge
advantage, this framework was hence selected for further use. Caffe indeed comes with a few types of
solvers which can be used for training or testing but they lack the ability to classify unknown
data. A third state would be needed in addition to TRAIN and TEST. We call the missing phase GUESS
internally within our application.

The following section describes the implementation details of the Caffe framework. It is essential
that those details are understood in order to use and continue in developing of our classifier.


\subsection{Caffe Blob and Synced Memory} % (fold)
\label{design:sub:caffe_synced_memory}

Synced Memory is the bedrock of Caffe's design. It provides seamless manipulation of GPU and CPU
memory by implementing lazy synchronization so it mitigates any unnecessary memory movements until
the memory is not actually needed.

\begin{figure}[htp]
	\centering
	\includegraphics[scale=0.35]{drawings/caffe-memory-uml.pdf}
	\caption{UML class diagram of Net - Memory relation}
	\label{design:fig:caffe_memory_uml}
\end{figure}

Figure \ref{design:fig:caffe_memory_uml} shows how \texttt{Blob} and \texttt{SyncedMemory} are
positioned in Caffe's design. As we mentioned earlier, every blob is defined by a quadruple
\emph{(num, channel, height, width)} where every element is called an \emph{axis} so we can think
about it as a 4 dimensional object. The underlying implementation is a simple one dimensional array where data are continuous in width and therefore a position of an element is computed as
\begin{equation}
((n * \text{channels} + c) * \text{height} + h) * \text{width} + w
\end{equation}
where the one-letter variables denote the position of the element and the words are constants for a
given Blob.

The primary aim of blobs is to hold image data. For our data we are using blobs of shape $(N, 1, 1,
W)$ where $W$ is maximal width of spectra and $N$ is number of spectra per training batch. $N$
should be optimized such as it fits into memory of all GPUs. The maximal memory consumption on GPUs
is defined by a constant in the compilation phase of Caffe.

% subsection caffe_blob_and_synced_memory (end)


\subsection{Caffe Layers} % (fold)
\label{design:ssub:caffe_layers}

We will start from memory handling. Every layer keeps its \emph{parameters} (as the Caffe
developers call it) in caffe::Blobs within the layer itself. Those parameters are stored in a vector
called \texttt{blobs\_} but a developer rarely touches them. This is the only thing which gets
serialized and restored from a layer so it has appropriate definition in a \texttt{proto} file.

Even though there are ``bottom'' and ``top'' blobs mentioned in the layer's definition, the layer
does not own them. Those blobs have to have unique names and that is set within layer definition in
config file by attributes \emph{bottom} and \emph{top}. The connection between layers is then
deduced from the blob names. If a layer B defines its bottom blob with the same name as some other
layer A defined its top blob, then a connection A->B is created. Moreover, the result of a layer can
be broadcast to many layers. In the implementation, the layers do not posses their top and bottom
blobs. Those blobs are owned by the wrapping \texttt{caffe::Net} object and passed to layers when
doing forward or backward pass.

There are no weighted connections between layers as one would expect. Caffe's solution is that the
full connection is implemented as a special layer \texttt{caffe::InnerProductLayer}. This solution
greatly simplifies the implementation of generic layers and wrapping \texttt{caffe::Net} class as well.

When a layer is instantiated by a \texttt{caffe::Net} object it reports its top and bottom blob
names. Those blobs are then allocated in the Net and referenced as possible top and bottom blobs. If
there is no bottom blob that would make use of a top blob then the top blob is marked as
\emph{output\_blobs} and can be accessed via function \texttt{caffe::Net::output\_blobs}.

The network structure is usually described by a graph of layers and data. Here we show the simplest
neural network defined using Caffe.

\begin{figure}[htp]
	\centering
	\includegraphics[clip=true, trim=2cm 3cm 5cm 12cm, scale=0.45]{drawings/caffe-net.pdf}
	\caption{Graph of very simple neural network defined using Caffe}
	\label{design:fig:caffe_net}
\end{figure}

The label object which goes directly from input layer to the output layers is a \texttt{blob} of
size \texttt{(batch\_size, 1, 1, 1)} therefore the label is put into one neuron as it is and all the
output layers will encode it to \texttt{one-hot} encoding. On the contrary, which does make sense, the output
of a network is expected to be a blob of size $(N, C, 1, 1)$ where $N$ is the \emph{batch\_size} and
$C$ is a number of distinct classes.

The labels and data are produced by \texttt{<Something>Data} layers and are expected to be in
separate Blobs. By implementation the ordering matters here -- data first, labels last. Having more
blobs in forward and forward functions is made possible by the parameters to those functions
which are vector of blobs.

\paragraph{MemoryData} layer is one of the possible input layer of our application. It is expected
to have 2 \emph{top blobs} as any other \emph{<Something>DataLayer}. The setup phase expect to have
\texttt{MemoryDataParameter} protocol buffer message as an input. This message doesn't do anything but
specify the shape \texttt{(batch\_size, channels, height, width)}. We spare the user of specifying
those and we compute them automatically.

Internally the label Blob has shape $(N, 1, 1, 1)$ where $N$ is number of samples for the neural
network. The important note is that \emph{batch\_size} which we know from the protobuf configuration
file for neural network (not solver) finetunes how much data will get transferred to GPU memory.
Therefore, there is a crucial condition $\mod(N, \mathrm{batch_size}) == 0$ in the official version which can
not be omitted. We have implemented shrinkage of the source data in the last batch but it turned out
that convolutional layers require the batch size to be constant.

\paragraph{InnerProduct Layer} is the name for what widely known as fully connected layer. In this
and convolutional layer we can define a weight initialization algorithm which has been proven as
significant part in constructing neural networks. The reason is that we have to initialize weights
in such manner that no information (input) gets amplified or muted. Therefore if we take the weights
as matrix, then its eigenvalues should be close to 1.
This layer is often the last one in the prediction part because it transforms any shape into
\texttt{(batch\_size, num\_outputs, 1, 1)} where \texttt{num\_outputs} is a parameter of the network.
If the layers is the one providing final prediction then \texttt{num\_outputs} has to be equal
to the number of classes in the prediction.

\paragraph{Accuracy Layer} is intended as output layer. It expects two bottom blobs -- label and
1hot encoded prediction. Suppose we have $C$ distinct classes therefore the prediction has to be in
shape of \texttt{(batch\_size, C, 1, 1)}. The layer takes optional parameter \emph{top-k} so it
returns a success if the correct label appears in the \emph{top-k} results.


\subsection{Caffe Net} % (fold)
\label{design:sub:caffe_net}

The network object is a container for layers. It possesses the data flowing through the network and
moves them from one layer to another. Moreover, it offers interface to access layers and blobs by
their name and function (for example output blobs).

% subsection caffe_net (end)

\subsubsection{Caffe Solver} % (fold)
\label{design:ssub:caffe_solver}

Solver is the component updating weight thus using the gradients in the network. Not all layers have
parameters to update but they still provide gradient values.  The update rule differs between
solvers. The basic solvers are \texttt{SDG} (stochastic gradient descent), Nesterov and AdaGrad. All
of the solvers are using common basic parameters such as momentum $\mu$ and learning coefficient
$\alpha$. The basic weight update is

$$ W_{t+1} = W_{t} + (\mu \Delta W_{t} + \alpha \Delta W_{t+1}) $$
%
where $\Delta W_{t}$ is weight update at time $t$.

A good strategy for deep learning with SGD is to initialize the learning rate $\alpha$ to a value
around $\alpha \simeq 0.01 = 10^{-2}$, and dropping it by a constant factor (e.g., 10) throughout
training when the loss begins to reach an apparent ``plateau'', repeating this several times.
Generally, the momentum should be close to $1$ in order to decay the learning rate slowly. Usually a
good value is $\mu=0.9$. By smoothing the weight updates across iterations, momentum tends to make
deep learning with SGD both stabler and faster. This was the strategy used by Krizhevsky et al. in
their famously winning CNN entry to the ILSVRC-2012 competition. Note that the momentum setting
$\mu$ effectively multiplies the weights updates by a factor of $\frac{1}{1-\mu}$. It is a
recommended technique to decrease the learning rate $\alpha$ meanwhile increasing $\mu$
\cite{caffe_web}

Our case of classifying spectral shapes is a problem of searching for exactly one out of K exclusive
classes. Therefore the end of our network has exactly $K$ neurons followed by a \emph{Softmax loss
layer} which is a multinomial logistic regression and transforms variadic values into probabilities.There are more possible loss functions such as
\begin{compactitem}
	\item Sigmoid Cross-Entropy Loss -- predicts K independent values in range of $(0,1)$
	\item Euclidean Loss -- regressing real-valued labels (possibly infinite classes distributed in
	continuous space) $(-\infty, +\infty)$
\end{compactitem}

% subsubsection caffe_solver (end)

% section caffe_neural_layer (end)


\section{Application usage} % (fold)
\label{design:sec:application_usage}

The application is configurable via JSON files with sections designated to surrounding server. We
provide a commandline interface with few arguments.
\begin{verbatim}
	./vodigger --train <repository>
	./vodigger --test <model-snapshot> <repository>
	./vodigger --time <repository>
	./vodigger --dump <model-snapshot> <repository>
\end{verbatim}
The mandatory argument for all parameters is the repository in which the classifier operates. It has to
be a folder with a configuration file named \texttt{config.json} by default.
\begin{figure}[htp]
\begin{verbatim}
{
    "name": "classifier_name",
    "parameters":
    {
        "mode": "GPU",
        "solver": "solver.prototxt",
        "model": "model.prototxt",
        "test_iters": 1,
        "bench_iters": 20
    }
}
\end{verbatim}
\caption{The minimal configuration file of our classifier}
\end{figure}

The parameter \texttt{model} and \texttt{solver} are very similar. They contain names of a model and
solver definition files, respectively. Both files has to be proto files following the requirements for Caffe model and solver definition, respectively. There are
two differences. First is, that we set up the running mode (either CPU or GPU) directly in this
config file (by \texttt{parameters.mode}) and not in a solver. The reason is simply that the solver
is not always needed and therefore it should not be responsibility of a solver to set up the mode.
Second difference is that we have two test\_iteration values -- one in solver and second in config
file. The one in solver says number of test iterations while training a network (for validation).
The \texttt{test\_iter} parameter in the config file determines a number of iterations when the
classifier is running in \texttt{--test} mode. If the networks outputs ArgMax layer in testing phase
the classifier creates a confusion matrix out of it. Last parameter \texttt{bench\_size} denotes how
many iterations of forward-backward pass (in training mode) should be done in order to benchmark
given model.

% section application_usage (end)
