In this chapter we provide a state-of-the-art overview based on the most recent conference
proceedings and science publications. Many current applications are using specialised FPGA chips but
that is not the aim of this thesis. Our goal is to use \acrlong{GPGPU} (GPGPU) for classification so
that our methods can be reused. We will omit using massive parallel technologies like MPI because
machine learning tasks usually requires huge data flows which is a weak spot of distributed
computations.

Our survey of literature is summarised in figure \ref{fig:par-ml-history} and shows dates of
publications of \acrshort{GPU} implementations of \acrshort{ML} algorithms. There was a tremendous
increase of interest in this area starting in 2005. It might be linked to the unveiling of CUDA
technology (officially) in 2006.

\begin{figure}[htp]
	\includegraphics[width=\textwidth]{drawings/GPU-ML-history.pdf}
	\caption{Dates of GPU implementation of ML algorithms}
	\label{fig:par-ml-history}
\end{figure}

Following list is a summary of feasibility of \acrshort{ML} algorithms for parallelization. The
most suitable algorithms appear first in the list.
%
\begin{enumerate}
	\item \emph{Particle swarm optimization} -- Parallelism is achieved through propagating global best solution slowly through neighboring particles. Even then there is approx. 300 times speedup \cite{Mussi:2011:GAP:2001576.2001786}.
	\item \emph{Genetic algorithms} -- Island model has the highest speedup factor (cca 55) \cite{Ga.Island.2012} over CPU and scales linearly with number of GPUs.
	\item \emph{Neural networks} -- Ease of parallelism depends on interconnection between layers, the bottleneck is the learning of fully-connected layers \cite{DBLP:Krizhevsky14}.
	\item \emph{Self organizing maps} -- They suffer from all-to-all communication in learning phase which can be partially suppressed by using batch learning. It will move the communication at the end of every batch resulting in 44 times speedup \cite{SOM.2012}.
	\item \emph{SVM} -- It is possible to learn SVM in smaller batches with an iterative version of Newton SVM algorithm. The speed-up achieved with this technique was about 45 times \cite{parallel_SVM:2008}.
	\item \emph{Random forest} -- It is hard to efficiently implement parallel learning for general purpose trees \cite{RandomForest:2013}.
	\item \emph{Clustering} -- Markovian chains and graph transformations are leading approaches in clustering but both are hardly decomposable.
\end{enumerate}


\section{Genetic algorithms}

GAs are algorithms for local search in a huge state space. The principle is that we encode the
solution of a problem into a ``genome'' of a gen and then mutate gens between each other and keep
only the best solutions found so far. The algorithm consist of three phases:

\begin{compactitem}
	\item \emph{crossover} -- select two random genes and perform a genetic crossover
	\item \emph{mutation} -- select a random gene and mutate random parts of its genome
	\item \emph{evaluation} -- compute fitness for each gen (distance from the good solution)
\end{compactitem}
%
As one can see GA has great potential for massive parallelization. There are three basic types of
parallel genetic algorithms \cite{Xu:2005:SPG:1068009.1068145}, \cite{Alba.Tomasiny:2002}:
%
\begin{compactitem}
    \item \emph{Master-slave} -- One single processor performs the genetic operations and uses
other processors for evaluation of individuals only. This model is useful when dealing with a small
number of processors or with computationally intensive evaluations.
%
    \item \emph{Island model} -- In this model, every processor runs an independent evolutionary
algorithm (EA) using a separate sub-population. The processors cooperate by regularly exchanging
migrants (good individuals). The island model is particularly suitable for computer clusters, as
communication is limited.
%
    \item \emph{Diffusion model} -- Here, the individuals are spatially arranged, and mate with other
individuals from the local neighborhood. When parallelized, there is a lot of inter-processor
communication (as every individual has to communicate with its neighbors in every iteration), but
the communication is only local. Thus this paradigm is particularly suitable for massively parallel
computers with a fast local intercommunication network.
\end{compactitem}


One of the functional and general purpose implementation is done in project GAME\cite{GAME}. The
genetic algorithm in GAME can be used as a standalone model or can be incorporated into MLP as a
support function for finding the best coefficients for links between neurons.


\section{Particle Swarm Optimization} % (fold)
\label{survey:sec:particle_swarm_optimization}

Particle Swarm Optimization (PSO) is naturally decomposable local search algorithm. The main idea is
that we introduce multiple agents (particles) and put them randomly in our state space. Those
particles are evaluating the state they are at and they keep moving with some velocity in the state
space. The direction is randomly set at the beginning but it is constantly forced towards the best
solution found so far. One can see that this forces synchronization into the algorithm. Fortunately
the synchronization can be weak here without any harm to the speed of convergence to the best
solution.

All locals searchers can be unsynchronized and still follow the latest maximum as it is shown in
work \cite{Mussi:2011:GAP:2001576.2001786} which compares CPU and GPU implementations. There are too
many limitations of the traditional von Neumann architecture which prohibit effective parallel
implementation. Fortunately the GPU architecture suits this algorithm well.

New improvements to the original PSO algorithm \cite{PSO:1995} include the notion of
\emph{unhealthiness} to describe swarms or sub-swarms stuck at local optima, then applying random
mutations to the unhealthy particles' positions.

Almost all recent GPU implementations assign one thread to each particle which, in turn, means that
fitness evaluation has to be synchronized after every iteration. The latest version of GPU PSO
algorithm brings two improvements on how to speedup the computation. First is allocation of a thread
block per particle, each of which executes a thread per problem dimension. This way every particle
evaluates its fitness function and updates position, velocity, and personal best for each dimension
in parallel. Second is removal of the need to store and maintain the global best in global memory.
Every particle checks its K neighbours' personal best fitnesses, then updates its own personal best
in global memory only if it is better than the previously found personal best fitness. This can
speed up execution time dramatically \cite{Mussi:2011:GAP:2001576.2001786}.

% section particle_swarm_optimization (end)


\section{Neural networks} % (fold)
\label{survey:sec:neural_networks}

Neural network can be viewed in two ways. The first one is as many nested (non)linear functions. The
second one is that neural network is a sequence of matrices multiplications. Both views are valid
and both suggest different ways of parallelization of the problem. There is one common obstacle for
both views and that is the necessity to synchronize the results of previous operation. This
requirement is indeed weakened by ``interconnection'' of the levels of computation but in the most
common case there is full connection which means that the following phase has to wait for all results
from the previous phase.

Today, most of neural networks are at least multi layer neural networks. Only this type of network
is able to solve the XOR and harder problems. Neural networks are good candidates for massive
parallelization since there has to be as many neurons as the inputs which are usually (nearly) fully
connected to neurons in subsequent layer. The complexity of the whole structure grows exponentially
with number of hidden layers that we usually suppose to be up to three fully-connected hidden layers.

There are many possibilities of implementing the parallel training phase which is the most
computationally demanding one. We will describe basic ideas of parallelization mostly proposed by
Krizhevsky and Yann Le Cun.

Recently there was a big boom of publicly available GPU implementations of convolutional networks
which are practically a synonym of deep neural networks. This can be stated because the convolutional
network is required to have more types of layers than just the convolutional one. Usually there is a
max-pooling layer which de-noise feature maps and then there is always one or more non-linear
transformations in order to scale the data properly followed by fully-connected layer to produce the
right amount of outputs. Therefore improving convolutional networks mean improving all types of
neural networks too.

The number of open source projects has increased together with popularity of the deep neural
networks around 2011. The most advanced technologies now are NVIDIA \texttt{cuDNN}, \texttt{Caffe},
\texttt{cuda-convnet2}, \texttt{Torch}, \texttt{NervanaSys} and Facebook's \texttt{fbfft}. We
describe most of them in greater detail in Section
\ref{survey:sec:available_technologies}. The main leaders of research in this area are  Yann Le Cun,
Yoshua Bengio, Ilya Sutskever, Alex Krizhevsky and Geoffrey Hinton.

In the scientific area there are few GPU enabled solutions. One of them is DAME environment
developed by Università di Napoli Federico II. They have implemented an MLP using GPU with speedup
value around 8 over a sequential CPU version of similar computational category
\cite{DAME.Brescia:2014}. The resulting application is called \texttt{DameWare} which is a whole
platform for data discovery with web UI interface, scheduler and many implemented machine learning
algorithms including the previously mentioned one.

Another interesting and brand new area (1996) of neural networks is \emph{spiking neural networks}
\cite{sNN-1996}. They are called the $3^{rd}$ generation of neurons. Those networks do not have
per-layer synchronization as classical artificial neural networks but their neurons ``fire''
whenever they have enough input. Obviously this approach resembles more the real brain and therefore
it is claimed to have better accuracy in classification and decision making. Moreover spiking
networks take time into account and therefore it can decide on more kinds of problems than artificial
neural network. Since there is no synchronization involved then modelling of such networks is quite
straightforward either using simple electrical circuits or GPUs.

The recent parallelization approaches are distinct for every type of layers. The layers are viewed
as separate models. The parallelization techniques are discussed more in detail in chapter
\ref{scalability}.


\section{Association analysis} % (fold) \label{survey:association_analysis}

The association rule learning was the first data mining area that has been implemented on special-
purpose hardware (probably because it is very useful in client classification in banking and
marketing area). Unfortunately this algorithm is not useful in our case.

% section association_analysis (end)


\section{Clustering - unsupervised classification} % (fold)
\label{survey:sec:clustering_unsupervised_classification}

The basics of clustering algorithm is to search for similarities between the data. That implies a lot
of communication since every new unit has to be compared with all the other pieces of data we already
have.

There are many theses and articles claiming parallelization of basic SCAN algorithm
\cite{GPUSCAN.2014}. The SCAN algorithm is a clustering algorithm using core-nodes which are well
defined by two parameters $\epsilon$ and $\mu$ where the first one stands for a maximal radius to
search for nodes and the later one expresses how many nodes has to be in the neighbourhood for a
node to be considered a core- node. The way to scale this algorithm is to sort edges definitions for
core-nodes labelling and then use sub-trees in the graph for actual clustering called ``linking'' in
case of SCAN algorithm. This algorithm supposes a graph structure as its input. There are indeed new
methods of transforming any input data into a graph structure. We describe those in details in the
sections bellow.


\paragraph{Markov Clustering} is a decomposable clustering algorithm mainly used in bioinformatics.
MCL uses two simple algebraic operations, expansion and inflation, on the stochastic (Markov) matrix
associated with a graph. The Markov matrix $M$ associated with a graph $G$ is defined by normalizing
all columns of the adjacency matrix of $G$. The clustering process simulates random walks (or flow)
within the graph using expansion operations, and then strengthens the flow where it is already
strong, or weakens it where it is already weak using inflation operations. The application of
expansions and inflations creates regions with strong internal flow (clusters) separated by
boundaries within which flow is absent \cite{Markov.Clustering.2008}.


\paragraph{K-Means} as the original algorithm was recently extended in \texttt{k-means++} by Arthur
and Vassilvitskii \cite{k-means-pp}. Since their work enable parallelization and streaming
processing, many practical implementations follow. There were also custom hardware implementations
which were, for example, clustering colour in images in realtime by generating the \texttt{kd-trees}
dynamically on the \gls{FPGA}. Finally, Ma et al. \cite{gpu-k-means} proposed a processing structure
especially for GPUs that can be efficiently utilized in a wide range of data mining algorithms, like
\texttt{k-means} clustering or EM clustering \cite{Chrysos:2013:HPS:2400682.2400706}.

% section clustering_unsupervised_classification (end)


\section{Classification and Regression Trees}

This approach seems to be very popular in past few years. The first attempts towards parallelization
of the decision tree induction led to the proposal of two efficient software-based solutions\cite{Chrysos:2013:HPS:2400682.2400706}:

\begin{enumerate}
    \item SPRINT which handle massive datasets by changing the CART \cite{CART} algorithm's nature
    \item SLIQ which is trying to change the way the data are stored in the memory
\end{enumerate}

The SPRINT algorithm implemented the same split selection method as the one utilized in the CART
algorithm, and it is considered to be the successor of the SLIQ algorithm. The SPRINT and the SLIQ
algorithms achieved an almost linear speedup with respect to the number of CPUs and the sample size.


\section{Random forest} % (fold)
\label{survey:sub:random_forest}

Random forest is a classifier consisting of a collection of tree-structured classifiers
$\{h(x,\Omega_k ), k=1,... \}$ where the $\{\Omega_k\}$ are independent identically distributed
random vectors and each tree casts a unit vote for the most popular class at input $x$
\cite{Random.Forests.origin.2001}.

Random forests can be parallelized as shown in \cite{10.1109/ICDE.1999.754925} even though the author
does not precisely follow the ``random approach''. He uses dynamic sub-tree partitioning for
higher throughput. It is usable for a random forest or a tree with high degree. Random forest is a
model of many trees trained using sub-samples of the training data such that each sub-sample contains
subset of input attributes. Thanks to this approach we obtain mostly uncorrelated trees which we put
together using ensemble method bagging. There is a lot of active research around this method, mostly
at Microsoft and Bell labs.

Parallel version of random forests has been already tried with success on the same data as we have
\cite{palicka-2014}.

% subsection random_forest (end)
% section classification_and_regression_trees (end)


\section{Self organizing maps} % (fold)
\label{survey:sec:self_organizing_maps}

SOM allow sampling of a n dimensional set in a topological map of lower dimensionality which keeps
in its topology similarities between data.

The map is constituted of a set of n-dimensional structures that are called neurons. A set of data
with same dimensions (weights) as the neurons is presented to the map one by one intending to be
grouped. For each showed input, two distinct steps are made, the first is where we compute a
function around the map to discover the most similar neuron with the input according to this
function. We call this most similar neuron the Best Matching Unit (BMU). A common function to be
applied is the Euclidean distance. The second step is where we propagate the input characteristics
on the neighbourhood of the BMU. On every neuron of the map equation \ref{eq:som} is applied

\begin{equation} \label{eq:som}
	w_i (t + 1) = w_i (t) + h_{ci} [x(t) - w_i (t)]
\end{equation}
%
where $w_i$ is the weights vector of the neuron, $x(t)$ is the presented input at time $t$ and
$h_{ci}$ is a function that decays exponentially in function of the distance between winner neuron
and the updating one. This function also depends on a learning rate which decays at each iteration.
After a group of inputs is presented, the map should converge to the existing clusters on the data
set. In addition, the more similar clusters should also stay closer \cite{SOM.2012}.

Parallel version of SOM was implemented on the same data by a bachelor's thesis [Lukáš Lopatovský]
% section self_organizing_maps (end)


\section{Support Vector Machines} % (fold)
\label{survey:svm}

Very popular method for classification of single class (either accept or reject the class). SVM can
use linear separator of the state space or use kernel trick to use more complex separation of the
space (e.g. spherical separation). Since the learning of SVM model requires computation of distance
to each training sample it is easy to parallelize. The input can be split into smaller chunks which
are evaluated independently and after that a reduction function is used. There are interesting
parallel approaches such as using iterative learning in order to reduce memory consumption or Newton
SVM which optimize different than QP function as shown in work \cite{parallel_SVM:2008}. This
implementation showed performance as much as 100 times faster than sequential \texttt{LibSVM}.

% section svm (end)


\section{MapReduce Framework} % (fold)
\label{survey:sec:mapreduce_framework}

Finally, there are certain papers that combine software frameworks, such as \texttt{MapReduce}, and
hardware platforms, such as \gls{FPGA} and \gls{GPU}, for the acceleration of data mining
algorithms. It provides programming abstraction, hardware architecture, and basic building blocks to
developers. A \texttt{Rankboost} algorithm was implemented in this framework, which is an effective
ranking algorithm and it is widely used in applications which involve data mining tasks. The
implementation achieved approximately 30 times better performance when compared to the performance
of a CPU-based implementation \cite{Chrysos:2013:HPS:2400682.2400706}.

Unfortunately this technology is limited in complexity of executed code so it would not make sense to
use it on more complicated models. However the underlying technology of distributed computing can
be used to further expand data throughput but not data parallelism.

%section mapreduce_framework (end)


\section{Available technologies} % (fold)
\label{survey:sec:available_technologies}

We are considering only open source implementations of \acrshort{ML} algorithms ideally backed up by
some academic publication.

\subsection{MLlib} % (fold)
\label{survey:ssub:mllib}

Increasingly popular library developed by UC Berkeley for Distributed Machine learning running on
\texttt{Apache Hadoop} and \texttt{Spark}. It contains most of the machine learning algorithms. It
uses \texttt{Fortran} binaries to increase its performance and so far it is tightly bounded to CPU
architecture. This library is in its early stage of development. It is getting popular mostly
because of industry shift to concurrent data-crunching platforms such as \texttt{Spark}.

% subsection mllib (end)

\subsection{GPUMlib} % (fold)
\label{survey:gpumlib}

A new library still under active development. The inception of the library was at academical grounds
of Portugal in 2011 by the paper \cite{GPUMLib.2011}. As they claim in the paper there is no
standard and easy-to-use library providing GPU implementation of the most known machine learning
algorithms. The aim of this project is to become such a standard library. The library is written
using newest standard \texttt{\gls{C++11}} in combination with the latest \texttt{CUDA toolkit}. The
library already contains many popular models like \emph{Radial basis function}, \emph{Restricted
Bolzmann machine}, \emph{SVM} and many more. Nevertheless it lacks more complicated models so there
is still a lot of space for improvement.

On that note we can justify our decision not to use this library as one of our building blocks since
in our opinion the project of \texttt{GPUMlib} will never go into areas of deep learning since there
are many specialized frameworks for that such as \texttt{Torch}, \texttt{Caffe}, \texttt{cuDNN},
\texttt{NervanaSys} etc.

% subsection gpumlib (end)


\subsection{cuda-convnet(2)} % (fold)
\label{survey:ssub:cuda_convnet}

It's a single purpose library implementing convolutional networks in NVIDIA CUDA framework. It uses
pure C for the model but the data feeder is written in Python. In my opinion the documentation of
data loading is insufficient so it is hard to start with the library. After initial struggles one
can find out that there are three evolutions of the implementation. Every evolution contain many
important optimizations by Alex Krizhevsky who won many times an international competition with this
implementation.

This library is used by important CUDA machine learning frameworks such as \texttt{Caffe} and
Torch. There are two important sources of code. First, the original by Alex
Krizhevsky\footnote{\url{https://code.google.com/p/cuda-convnet/}} and its improved
version\footnote{\url{https://code.google.com/p/cuda-convnet2/}}. There is a
fork\footnote{\url{https://github.com/dnouri/cuda-convnet}} promoted by NVIDIA. It enhances the
original with \texttt{dropout} layer and many standard CUDA libraries.

% subsection cuda_convnet (end)


\subsection{NVIDIA cuDNN} % (fold)
\label{survey:ssub:nvidia_cudnn}

The NVIDIA CUDA Deep Neural Network library (\texttt{cuDNN}) is a GPU accelerated library of
primitives for deep neural networks. It emphasizes performance, ease-of-use, and low memory
overhead. \texttt{cuDNN} is designed to be integrated into higher-level machine learning frameworks,
such as the popular \texttt{Caffe}, \texttt{Theano}, or \texttt{Torch} software frameworks
\cite{cuDNN}.

NVIDIA claims that \texttt{cuDNN} accelerate \texttt{Caffe} convolutional layer by factor of 1.2 -- 3. They
give example of AlexNet Layer 2 where the forward phase had: 1.9x faster convolution, 2.7x faster
pooling using \texttt{cuDNN} over their native implementation \cite{cuda65-performance}.

% subsection nvidia_cudnn (end)


\subsection{Torch} % (fold)
\label{survey:sub:torch}

An academic framework built in Switzerland. The underlying concept is LuaJIT which is compiled into
C/CUDA code in the end. This framework contains many machine learning functions and models and
therefore it is widely used by professionals such as Yann LeCun. The framework implements models
such as neural networks, deep convolution networks, energy-based models and numerical optimization
routines just to name a few. However, a substantial amount of models are still the original
implementations in C/C++  with light wrapper around them. Torch is mainly used by Facebook for their
own AI research with NN implementations such as \texttt{fbnn}, \texttt{fbcunn} and \texttt{fbfft}.

% subsection Torch (end)

\subsection{Theano} % (fold)
\label{survey:sub:theano}

It is a Python framework built on top of \texttt{NumPy} and \texttt{cuDNN}. Theano is an open source
project primarily developed by a machine learning group at the Université de Montréal. Its primary
aim is to compile mathematical expressions in Python into machine code. This combines the
convenience of NumPy's syntax with the speed of optimized native machine language \cite{theano}.

The most famous Theano user is the team behind site
\texttt{deeplearning.org}\footnote{\url{http://deeplearning.net}} which promotes deep learning since
2006. We decided not to use it for its python bindings. If we are going to implement a parallel
model we need a fine control over memory and how the parallelism is done. We would need to change
the core of Theano rather than just tweaking bits of code.

% subsection theano (end)


\subsection{Caffe} % (fold)
\label{survey:sub:caffe}

Caffe provides multimedia scientists and practitioners with a clean and modifiable
framework for state-of-the-art deep learning algorithms and a collection of reference models. The
framework is a BSD-licensed C++ library with Python and MATLAB bindings for training and deploying
general-purpose convolutional neural networks and other deep models efficiently on commodity
architectures. Caffe is maintained and developed by the Berkeley Vision and Learning Center
(BVLC) with the help of an active community of contributors on GitHub \cite{jia2014caffe}.

Many machine learning applications are based on this framework. It is not because of its speed,
being overall average. It is because of its nice design and ease of incorporation into existing
applications. We can choose few examples as NVIDIA DIGITS or ``Brain simulator'' by Keen Software
House. NVIDIA is surprisingly active in development of this framework.

Caffe started as a PhD thesis and it was released to public in September 2014. It claims to have
high performance convolution and that was its main focus -- convolutional neural networks. The
latest update (release 2) brought fully-connected and recurrent layers, contrast normalization and
many improvements to convolutional networks. The following update has promised multi-GPU support
(backed by NVIDIA itself) which makes Caffe a safe bet for the future.

Even though it might seem that current lack of support for multi GPU is a deal breaker it is not so.
The only disadvantage is that if we want to use many GPUs we need to write the application in
distributed manner. Or even parallel implementation is sufficient. The code on CPU side will get
more complicated but the result will be very similar as if it ran on many GPUs natively.

% subsection caffe (end)


\subsection{NVIDIA DIGITS} % (fold)
\label{survey:sub:nvidia_digits}

The NVIDIA Deep Learning GPU Training System (DIGITS) is a software built on top of \texttt{cuDNN}
and \texttt{Caffe} and released to public on 14th of March 2015. It provides user interface for
controlling, monitoring and analysing model's runtime and structure. The biggest advantage is
realtime monitoring of loss function and accuracy, which makes it perfect for testing out new
network architectures. The other useful feature is visualisation of intermediate layers. This
feature is mostly intended for convolutional layers but can be useful in any type of layer.

The key features are
\begin{compactitem}
	\item Visualize DNN topology and how training data activates your network
	\item Manage training of many DNNs in parallel on multi-GPU systems
	\item Simple setup and launch
	\item Import a wide variety of image formats and sources
	\item Monitor network training in real-time
	\item Open source, so DIGITS can be customized and extended as needed
\end{compactitem}

On the other hand DIGITS is strongly bound to image data and therefore it is of no use for us.
NVIDIA claims to support generic type of data in the future but is has not been released during
writing of this thesis.

% subsection nvidia_digits (end


\subsubsection{DAMEWARE Application} % (fold)
\label{ssub:dame_application}

DAME is a data mining infrastructure specialized for mining massive data sets. It offers complete
toolset of machine learning algorithms. The main entry point to the application is an web
application called Web Application REsource of DAME
(DAMEWARE\footnote{\url{http://dame.dsf.unina.it/dameware.html}}). It can handle common astronomical
data formats such as FITS and VOTable files. In release 1.0, the first parallel implementation of a
machine learning model, fast multi-layer perceptron (FMLPGA), was added. The implementation is based
on the GPGPU+CUDA environment, enabling a speedup of about 8 times. This algorithm extended already
broad collection of machine algorithms which contains

\begin{compactitem}
\item MLP + Back Propagation
\item MLP + Quasi Newton
\item MLP-LEMON (Levenberg-Marquardt Optimization Network) for classification/regression
\item Random Forest -- for multivariate classification and regression
\item Support Vector Machines (SVM)
\item K-means model (through KNIME engine)
\item CSOM (Clustering Self Organizing Feature Map) -- Unsupervised Image segmentation
\item GSOM (Generic Self Organizing Feature Map)  -- (Clustering) based on customized SOFM model
\item SOM (feature selection) + autoPostProcessing, K-means, Two Winners Linkage (TWL) or U-matrix with Connected Components (UmatCC)
\item ESOM Evolving SOM for clustering
\item Probabilistic Principal Surfaces (PPS) -- feature selection
\end{compactitem}

The application itself is not freely installable because it is bound to cluster architecture on
which it runs. There are, however, possibilities of own extensions via pluggable user data mining
models known as \texttt{dmplugin} client application. We aim to create such a plug-in from our own
implementation of one or two models. This would add the second massively parallel algorithm.

We were in contact with its key developer Dr. Massimo Brescia who was improving the application so
it fit our needs. During the period of writing this thesis DAME's import procedure was enhanced so
it accepts more than 500 columns. Furthermore the manual for MLP model has been extended with point
18 which is necessary for successful usage of the
model\footnote{\url{http://dame.dsf.unina.it/dameware.html\#mlpqnaman}}. There are ongoing
improvements such as realtime error output and few more.

% subsubsection dame_application (end)

% section available_technologies (end)


\section{Performance measurements} % (fold)
\label{sec:performance_measurements}

First we would like to point out some performance metrics measured by NVIDIA itself. We
expect those metrics to be slightly optimistic but still providing a good overview. The tested
hardware was K40c which has following configuration.\\
\\
\begin{tabular}{ l l }
GPU    & 2880 cores, clocks from 745 MHz up to 875 MHz (boost) \\
Memory & 12 GB of GDDR5 3.0 GHz, 288 GB/sec, 384-bit interface \\
Socket & PCI-E Gen3., 8 GT/s\footnote{Giga Transfers}, 985 MB/s per line (total 16 lines) \\
\end{tabular}
\\

CUDA Toolkit comes together with optimized libraries for mathematical operations. We will show
computational power then data throughput and finally we will compare with performance of other
components in computers and derive some optimization ideas.
%
\begin{figure}[htp]
{\centering
  \begin{tabular}{| l | l | r | r |}
    \hline
    library   &   problem      &  single precision  &   double precision \\ \hline
    cuFFT     &   1D,complex   &  700 GFLOPS        &   300 GFLOPS       \\ \hline
    cuBLAS    &   gemm         &  3000 GFLOPS       &   1200 GFLOPS      \\ \hline
    MKL       &   gemm         &  -                 &   200 GFLOPS       \\ \hline
  \end{tabular}}
  \\
  \\
  {\small GPU configuration: 6.5 on K40c, ECC ON, 28M--33M elements, input and output data on
device, exclude time to create ``plans'', computing} \\
{\small CPU configuration: MKL 11.0.4 on Intel IvyBridge single socket 12-core E5-2697 v2 @
2.70GHz}
  \caption{Performance metrics by NVIDIA}
  \label{nvidia-cuda-performance}
\end{figure}

Facebook did performance analysis of publicly available implementations of convolutional networks.
The performance report is available online \cite{cnn-benchmarks} and we enclose their results here
for relative comparison of frameworks.

\begin{figure}[ht]
\centering
\begin{tabular}{| l | r | r | r |}
\hline
\textbf{implementation}  &  \textbf{time}  & \textbf{forward} & \textbf{backward} \\ \hline
NervanaSys-16            &   97            &    30            &    67             \\ \hline
NervanaSys-32            &   109           &    31            &    78             \\ \hline
fbfft                    &   136           &    45            &    91             \\ \hline
cudaconvnet2             &   177           &    42            &    135            \\ \hline
CuDNN (R2)               &   231           &    70            &    161            \\ \hline
Caffe (native)           &   324           &    121           &    203            \\ \hline
Torch-7 (native)         &   342           &    132           &    210            \\ \hline
\end{tabular}
\caption{Showcase of relative performance of NN implementations}
\end{figure}
%
Our testing device, provided by Astronomical Institute of the Academy of Sciences of the Czech
republic, is an one-blade server with GeForce GTX 980 installed.\\
\\
\begin{tabular}{ l l }
GPU    & 2048 cores, clocks from 1126 MHz up to 1216 MHz (boost) \\
Memory & 4 GB of GDDR5 3.0 GHz, 224 GB/sec, 256-bit interface \\
Socket & PCI-E Gen3., 8 GT/s, 985 MB/s per line (16 in lines in total) \\
\end{tabular}
\\

% section performance_measurements (end)
