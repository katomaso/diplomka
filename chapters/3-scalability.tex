The only two possibilities in scalability are model parallelism and data parallelism. All other
options result in higher throughput but they won't scale the computation.
\begin{itemize}
	\item \emph{model parallelism} -- different workers train different parts of the model
	\item \emph{data parallelism} -- different workers train on different data examples
\end{itemize}

In model parallelism, whenever the model part (subset of neuron activities) trained by one worker
requires output from a model part trained by another worker, the two workers must synchronize. In
contrast, in data parallelism the workers must synchronize model parameters (or parameter
gradients) to ensure that they are training a consistent model \cite{DBLP:Krizhevsky14}.


\input{chapters/31-terms.tex}


\section{Parallelism by Alex Krizhevsky} % (fold)
\label{scale:sec:model_parallelism_by_alex_krizhevsky}

A good example of optimizing throughput is usage of batch processing. This approach together with
very clever model parallelization is implemented in \texttt{cuda-convnet2} by Alex Krizhevsky who
was leading benchmarks of convolutional networks for a long time. The optimizations, he is using,
are described in following sections. Those optimizations are not included directly in any other
implementation of neural networks (such as NVIDIA \texttt{cuDNN} or \texttt{Caffe}) but the \texttt
{cuda-convnet2} itself is a part of \texttt{Theano} and \texttt{Caffe} frameworks so one can benefit
of that indirectly.


\subsection{Fully connected layer} % (fold)
\label{scale:scalability:fully_connected_layer}

This layer is indeed the most used and the most demanding for communication. During learning and
recall phase it was observed that only $5-10\%$ of the time is actually spent on computing, while
the rest of the time is used on communicating the results. In order to scale the model we will count
with K workers (let's say K=32) and batch processing of data in batches of size N (e.g. N=128).
Therefore we rely on model parallelism here -- all workers process the same batch of the data on
different parts of the network. When the outcome (either activation value or gradient) is
computed, there are two ways of communicating it.

\begin{itemize}
    \item One of the workers sends its last-stage (computed on a batch of 128 examples) to all other
workers. All workers then compute the outcomes from the broadcasted last-stage and begin to
backpropagate their gradients for these 128 examples (in case of training) back to the sender.
\emph{In parallel with this computation}, the next worker sends its last-stage. The main
consequence of this is that much (i.e. $(K-1)/K$) of the communication can be hidden -- it
can be done in parallel with the computation of the fully-connected layers. This seems fantastic,
because this is by far the most significant communication in the network.

    \item This solution is very similar to the previous one. Each worker has computed the last-stage
for 128 examples. This 128-example batch was assembled from 128/K examples contributed by each
worker, so to distribute the gradients correctly we must reverse this operation. The rest proceeds
as in the previous solution. This one's advantage is that the communication- to-computation ratio is
constant in K. In the previous solution it was proportional to K therefore it was always bottlenecked
by the outbound bandwidth of \emph{the one} worker that had to send data at a given ``step''. This
solution enables the use of many workers for this task. This is a major advantage for large K.

\end{itemize}

%The biggest advantage of the parallel forward and backward pass, which we just introduced, is when
%the last-stage is for example from a convolutional layer and therefore it's independent on the other
%convolutional layers. Then one layer can proceed in back-propagating error or towards another batch
%of data.

\begin{figure}[htp]
\centering
\includegraphics[clip=true, trim=2cm 19.5cm 5cm 3cm, width=0.85\linewidth]{drawings/nn-par-train.pdf}
\caption{Illustration of a convolutional layer meeting fully-connected layers \label{scale:nn-par-train}}
\end{figure}

We are free either to update the fully-connected weights during each of the backward passes,
or to accumulate a gradient and then update the entire net's weights after the final backward pass.

% subsection fully_connected_layer (end)

\subsection{Convolutional layer} % (fold)
\label{scale:ssub:convolutional_layer}

This optimization is very similar to the one right above but with modifications to fit the more
independent nature of convolution.

When training convolutional nets in parallel, we rely heavily on data parallelism because
convolutional layers communicate their intermediate outcomes straight to one area instead of to all
neurons in the following layer as the fully-connected ones. Data parallelism means that we split the
input data between workers so that every worker has different data but trains the same layer.
Parallelization was done mainly by Krizhevsky in his papers \cite{cNN:2012} and \cite{DBLP:Krizhevsky14}.

Again we will count with parallelization using K workers and bath processing of size N data per
batch. The ideas come from the same source as in the fully-connected layer optimization. The
convolution layer is easier to parallelize because there is no inter-communication during
computation, only during weight update phase. The workers must also synchronize the weights (or
weight gradients) with one another so that:
\begin{enumerate}
	\item Each worker is designated 1/Kth of the gradient matrix to synchronize.
	\item Each worker accumulates the corresponding $1/K$th of the gradient from every other worker.
	\item Each worker broadcasts this accumulated $1/K$th of the gradient to every other worker.
\end{enumerate}

% subsection convolutional_layer (end)

\subsection{Softmax layer} % (fold)
\label{scale:par:softmax_layer}

A softmax layer normalize its values accoding to the maximal value. The definition of softmax
function is given in equation \ref{scale:eq:softmax} and shows that in order to compute the output
value $o$ at index $j$ the softmax function accesses all data $x$ in the input layer.
\begin{equation}\label{scale:eq:softmax}
	o_j(x_j) = e^{x_j}/\sum_i^N{e^{x_i}}
\end{equation}
N independent logistic units, specially trained to minimize cross-entropy, can be used to replace
this layer. This cost function performs equivalently to multinomial logistic regression but it is
easier to parallelize, because it does not require a normalization across classes. This is not an
important point with only 1000 classes, but with tens of thousands of classes, the cost of
normalization becomes noticeable.

% subsection softmax_layer (end)


\subsection{Asynchronous Stochastic Gradient Descent} % (fold)
\label{scale:sub:asynchronous_sdg}

Asynchronous SGD is an example of data parallelism. The original (non-stochastic) gradient descent
is a way to update parameters of a classifier in such a way that it minimizes the error produced by
the classifier called loss (or energy) function $\bigtriangledown E(W_t)$ where $W_t$ are network
parameters at time $t$. Equation \ref{scale:eq:gd} describes the update of the parameters with respect to the values of the parameters from the previous iteration and to the learning rate $\alpha$.
\begin{equation}\label{scale:eq:gd}
W_{(t+1)} = W_t - \alpha \bigtriangledown E(W_t)
\end{equation}
The loss function is computationally demanding and therefore we rather approximate it from a random sample of training data. Suppose we select $N$  random samples of $n$ elements each.
The stochastic gradient descent is computed as
\begin{align}
W_{(t+1)} = W_t - \alpha \bigtriangledown E_n(W_t) \\
\text{where} E(W_t) = \sum_{n=1}^N{E_n(W_t)}
\end{align}
The asynchronism can be introduced by split those samples into mini-batches and run those in parallel.

% subsection asynchronous_sdg (end)



% section model_parallelism_by_alex_krizhevsky (end)
