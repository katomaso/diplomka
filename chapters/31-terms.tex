\section{Terms and facts}

\paragraph{Parallelism} \label{scale:par:parallelism} is when the same part of a code is executed by more
processing units on one logical piece of data at the same time with implicit synchronization of the
data. Parallel processing can be achieved using threads (MPI), OpenMP or natively on GPU. Shared
memory is the most desirable state but in case of OpenMP we can work on distributed memory as well.
We can say that parallelism is a special case of concurrency.


\paragraph{Concurrency (distributed computing)} \label{scale:par:concurrency} is the case of one or
more programs processing one logical piece of data but with explicit synchronization by messages
passing. There is no implicitly shared memory, everything has to be explicit. Indeed, concurrency
can be running the same code for logically separated data or running two different codes because it
is still executing two tasks at once but without any synchronization, which is not a requirement for
concurrency. The most common architecture is multiple services running in distributed manner and
exchanging information over (UNIX) sockets. The approach that is most used is MapReduce framework
and its implementation by \texttt{Apache Hadoop}.


\paragraph{Neural Networks} \label{scale:par:neural_networks} is a sequence of (non)linear
layers connected to their successor. The connection has to be both ways because we use forward
connection in classification phase and backward connection in the learning phase. The connection
transmits value and multiplies it with \emph{a weight} associated to the connection. There are many
types of neurons, layers and weights-update strategies which we are going to be described briefly in
the following paragraphs.


\paragraph{Deep Neural Network} \label{scale:sub:deep_neural_network} is the most recent form of
neural networks. The term deep neural network is vaguely defined but for our purposes we will use
the definition: ``Deep neural network is a sequence of layers where there are at least two hidden
layers of a different type''. Deep networks introduce specialized layers such as convolutional
layer, dropout layer and pooling layers. Since the best known new-type layer is the convolutional
layer, then deep networks are quite often called convolutional networks. Those two terms have vaguely
the same meaning. The old-type multilayer neural network consisted of neurons with non-linear
activation functions connected by weighted connections to other neurons. Deep networks consider a
layer as the smallest computational unit and therefore connections between neurons in the old sense
do not exist. Any computation has to be done inside a layer (even weighting of inputs) and therefore connections between layers are direct and without weights.

\paragraph{Fully connected layer} \label{scale:sub:fully_connected} is a layer implementing
weighted connections in deep neural networks. It creates a fully connected bipartite graph between
its input and output layers.
\begin{figure}[htp]
	\centering
	\includegraphics[clip=true, trim=6cm 17.5cm 6cm 5cm, scale=0.4]{drawings/fully-connected-layer.pdf}
	\caption{Fully connected bipartite graph}
	\label{scale:fig:fully_connected_layer}
\end{figure}
%
Since every layer has to wait for the previous one in both passes (forward or backward) the
parallelization is hard. There were many experiments to overcome the synchronization problem but non
of them was successful. The only working solution is to feed the data in batches. Parallelization of
the model is impossible because every neuron has to communicate with all neurons from the previous
layer. We have to play with implementation to make the communication as less resource-consuming as
possible.


\paragraph{Convolutional layer} \label{scale:par:convolutional_layer} resembles retina in human's
eye and therefore is mainly used in image recognition. It can deal with rotation and translation of
features in images because they are searched for separately. The main idea is to create small
classifiers for every feature and then tile this classifier all over the image in a way that it
doesn't matter where a learned feature appears in the image. We can even rotate the small
classifiers in order to recognize not just translation but even rotation. That would indeed lead to
greater complexity of the output. Therefore \emph{pooling layers} were introduced. We describe them
in the following section \ref{scale:sub:pooling_layer}. In order to teach the small classifiers, a
technique called \emph{weight-sharing} is used.

The convolution itself is defined in one-dimensional space by Formula \ref{eq:conv1d}. The formula
defines output of a convolution $o[n]$ as a multiplication of a vector $f[n]$ with a convolutional
vector $g$ of size $M$ where $n$ is a spatial index and $f[n]$ a slice of data from vector $f$
centred around the index $n$ and having the same size as $g$.
\begin{equation} \label{eq:conv1d}
o[n] = f[n]*g = \sum_{u=0}^{M} f[n+(u-M/2)] g[u]
\end{equation}

For example, when a convolutional network is trying to recognize faces, it looks for eyes, nose,
mouth and other facial features independently and then combine findings of those separate elements.
The combination procedure is usually implemented as max-pooling layer. In our example in figure
\ref{scale:fig:convolutional_layer} the network should obtain neural triggers for two eyes, nose
and mouth which are positioned as they form a face. If it finds such an alignment it triggers a
neuron for face at the position, which is usually implemented by fully-connected layer.
Convolutional layer's features can be used in many ways. It does not need to be separate elements
but for example different lighting or angles of an object.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.85\textwidth]{drawings/conv-layer.png}
	\caption{Example of possible features and their maps}
	\label{scale:fig:convolutional_layer}
\end{figure}

To give a better image of how the convolutional layer works we have to consider that one feature is
looked up in the whole input image. For example, if we have an input image of size $N \times N$ and
a feature of size $k \times k$, the resulting feature map will be of size $(N-k+1) \times (N-k+1)$
in case of stride = 1. Every feature produces one \emph{feature map} which has to be connected
further so it creates many parallel networks.

Therefore there are many parallel implementations from which the best know is \texttt{cuda-convnet2}
by Alex Krizhevsky. Parallelization of convolutional layer is easier than fully- connected layer
because we train the same layer with different data. There is necessary communication when the layer
synchronizes weights which is negligible compared to the computation needed for obtaining the
weights.

The convolution produces massive amount of new data. Therefore \emph{stride} was introduced to tackle
this problem. Stride controls in what distant steps is the convolution applied. For example if the
stride is equal to the feature size then two following convolutions will not overlap. If the stride is
equal to 1 (which by default it is) then the convolution is moved by one pixel each time thus
overlapping as many times as the feature size.

%\paragraph convolutional_layer (end)


\paragraph{Pooling layer} \label{scale:sub:pooling_layer} is a simple layer which takes
maximal/average/minimal value from all neurons in a block. The main purpose of this layer is to
denoise the data and scale them down. Pooling is mostly used after convolution because convolution
multiply data by the factor of number of features. The variables in settings of this layer are
kernel $p$ and stride $s$. Kernel defines the pooled block size and stride the shift of kernel per
step. The inputting data are scaled down from size $(X, Y)$ to $((X-p)/s, (Y-p)/s)$.

% paragraph pooling_layer (end)

\paragraph{Dropout layer} \label{scale:sub:dropout_layer} randomly prevents some values from further
propagation. It is mainly an enhancement for learning and should not be used in the recognition
phase. The purpose is to avoid overfitting of the trained model on trained data.

% paragraph dropout_layer (end)


%\section{Random Forests} % (fold)
%\label{sub:random_forest}
%
%was first introduced by \cite{Breinman:2001}. The method builds trees using bagging principle with
%usage of (either decision or regression) trees. There is a difference in selection of training data
%(compared to building a single tree) where each tree is computed (using Gini index) by randomly
%selected dimensions of the data.
%
%% paragraph random_forest (end)
%% subsubsection ensemble_methods (end)
