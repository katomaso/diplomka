In this chapter we describe our contributions to Caffe framework by implementing a generic input
layer. The layer was not merged into the official version of
Caffe\footnote{\url{https://github.com/BVLC/caffe}}, but we plan to send a pull request when the
testing is done so we have a better chance for accepting our pull request.

\section{Caffe BigData Layer} % (fold)
\label{impl:sec:caffe_bigdatalayer}

We have developed a general purpose input layer for Caffe framework to allow bigger data to be
easily classified using deep learning. There are two problems with contemporary input layers from
the general-purpose point of view. The first one is that they require ``efficient'' storage formats
such as \texttt{leveldb}, \texttt{lmdb}, \texttt{HDF5} or other uncommon data formats. The second is
that the input layers takes \texttt{batch\_size} as input parameter which is not intuitive. Solution
to the second problem is straightforward -- we introduced input parameter \texttt{batch\_size} which
represents number of megabytes sent to a model every iteration. This parameter practically controls
how much data is send to GPU when the model runs in GPU mode. Solution to the second problem is
either to create utilities to convert any data into one of the efficient storages or to develop an
efficient layer for handling big text files from scratch. We decided that utilities would add only
more complexity and therefore we built a new input layer.

The layer provides cyclical read from its source file. Therefore during testing we can't go through
a file once. Instead we need to set up the number of iterations so it matches the number of data. To
our defence we can say that ``going through data once'' is not possible with any input data layer in
Caffe. It is given by Caffe's design decisions.

In order to speedup reading of inefficient CSV format the layer is using more threads and
furthermore it transform data into binary files and store them along with the original text files.
This transformation is performed meanwhile reading the textual format so no preprocessing is needed.
When we reach end of CSV file we automatically switch to reading the binary file. The binary file is
not deleted at the end so it can be reused next time. Changes in model (concerning batch size or
other than BigData layer) won't invalidate the binary file.We wanted to cache the data in memory but
since we are supposed to handle huge data we can not fit them all in RAM. However, if the file is
smaller than the \texttt{batch\_size} then we do cache the file.

\subsubsection{Performance} % (fold)
\label{impl:ssub:performance}

Figure \ref{fig:bigdata_performance} shows relative performance of BigData Layer compared to
\texttt{HDF5} input layer which is bundled within standard distribution of Caffe. The test was
performed using CPU and 0.5MB chunks of data. Data, solver and model were identical. The
measurements are average values from 4 repetition of the same experiment. The performance is linear
with respect to chunk sizes. The \texttt{BigData no cache} reads plain text CSV and transforms it
into binary file called cache meanwhile. The noticeable increase of performance happens when the
layer finishes reading from a flatfile and in the next iteration it starts reading from the
transformed binary file. The \emph{round trip time} is the time needed for the forward and backward
pass of a whole network (in this case it is the simple MLP network).

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth,clip=true,trim=1.5cm 1.5cm 1.5cm 18cm]{charts/bigdata-layer.pdf}
	\caption{Performance of BigData Layer compared to HDF5}
	\label{fig:bigdata_performance}
\end{figure}

Another optimization was added in order to lower disk usage. If the chunk size is greater than the
size of a source file then the data stay in memory and are not read from a disc over and over again.

% subsubsection performance (end)


\subsubsection{Configuration} % (fold)
\label{ssub:configuration}

Our BigData layer can be added to standard model definition as shown in Figure
\ref{fig:vb:bigdata-config}. It has many default values and therefore the only required are
\texttt{source}, \texttt{chunk\_size} and all data indices. The source can omit \texttt{label} index
which tells the network that data are to be classified.

\begin{figure}[htp]
\begin{verbatim}
layer {
  name: "traindata"    # name can be arbitrary
  top: "data"          # first layer will be used for data
  top: "label"         # second layer for labels
  type: "BigData"
  big_data_param {
    source: "spectra.train.csv"
    chunk_size: 1.5    # value in MB
    separator: " "     # separator is "," by default
    newline: "\n"      # newline is also "\n" by default
    header: 0          # skips # lines, by default 1
    # following indices are always inclusive and 0-based
    label: 0           # column index of label
    data_start: 1      # column index of data start
    data_end: 1863     # column index of data end
  }
}
\end{verbatim}
\caption{Example configuration of BigData layer}
\label{fig:vb:bigdata-config}
\end{figure}

There are few rules to the data format. Labels has to be integers from interval $<0,N>$ where $N$
is number of classes. The layer suppose 1D input data and therefore constructs blobs where all data
are put into \texttt{width} part of Blobs.

% subsubsection configuration (end)


\subsubsection{Memory consumption} % (fold)
\label{ssub:memory_consumption}

Unfortunately implementation of Caffe's GPU parallelism prevents us from putting really big data on
the graphic card. Caffe is using, in some layers, parallelism of such a degree that every
column/pixel is assigned one thread. If we take into account constraints imposed by CUDA 3.x then
we can have only $2^{16}$ blocks and $2^{10}$ threads per block. That means we can send a maximum of
64 MB in one batch. Newer graphic cards offer higher number of blocks so any model will work even on
bigger chunk of data than 64MB. Nevertheless we should count with the worst case.

64MB does not seem much but we have to take into account expansion of data. Every layer allocates
storage for its results so basically we have to multiply input data by number of layers. We
elaborate more precise memory consumption in equation \ref{eq:model_memory_consumption}. The
equation shows overall memory consumption $M$ based on size of inputting data. In the following
computations, we suppose that average depth of deep network is between 5 and 10 layers.
%
\begin{align}\label{eq:model_memory_consumption}
	M &= S_{input} * C_e * D + S_{weights} \\
	M &= 64 * 2.5 * (5, 10) + S_{weights} \nonumber \\
	M &= (800, 1600) + S_{weights} \nonumber
\end{align}
%
where $S_{input}$ is the input data size in megabytes (MB), $C_e$ is a \emph{network expansion
coefficient} which we discuss later, $D$ is depth of network for which we used a vector of two
values for usual min and max and $S_{weights}$ is negligible memory needed for storing weights of
connections. Note that average desktop graphics card has 3GB of global memory. So if we instantiate
$S_{input}$ with our limiting 64MB of data we will end up with memory consumption between 1GB and
1.5GB based on the depth of our network.

All the participants in equation \ref{eq:model_memory_consumption} are obvious except the
\emph{network expansion coefficient} $C_e$. We estimated that number of neurons per layer is in average
2.5 times more than neurons in the input layer. The estimated value 2.5 is based on LeNet and few
other standard networks. For example LeNet expansion coefficient is $\approx 3.2$ based on layer
sizes $[1.0, 14.7, 3.7, 4.1, 1.0, 0.6, 0.6, 0.01]$.

% subsubsection memory_consumption (end)

% section caffe_bigdatalayer (end)
